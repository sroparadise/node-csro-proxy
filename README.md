# node-csro-proxy
## NodeJS, ES6 middlewaring layer / server hybrid for MMORPG Silkroad Online.

### Prerequesities
- NodeJS LTS: https://nodejs.org/dist/v12.18.3/node-v12.18.3-x64.msi
- Silkroad Online game client & server  (CSRO-R version)
- Yarn package manager: `npm i -g yarn`
- PM2 `yarn add -g pm2`

### Setup
- Install dependencies `yarn`
- Setup `src/config/AgentServer.js`, `src/config/GatewayServer.js` and `src/config/DownloadServer.js` accordingly.
- Rename `.env.example` to `.env` and insert your database config accordingly to the sample provide (note that it is best to use connection over ip instead of named instances)
- Run `yarn install:db` to create (or drop & create again) the initial database.

### Launch in development mode
- GatewayServer `yarn dev:GatewayServer`
- AgentServer `yarn dev:AgentServer`
- DownloadServer `yarn dev:DownloadServer`

### Launch in production mode
- GatewayServer `yarn GatewayServer`
- AgentServer `yarn AgentServer`
- DownloadServer `yarn DownloadServer`
- All at once `pm2 start && pm2 dashboard`

Please see `.bat` files for more examples.

### Build
`yarn build`

## What else?
- packet docs https://github.com/DummkopfOfHachtenduden/SilkroadDoc
- silkroad-security https://github.com/EmirAzaiez/SilkroadSecurityJS
- SequelizeJS (for MSSQL) https://sequelize.org/master/manual/model-basics.html


Built with ♡ by Aye @ https://shiroi.online