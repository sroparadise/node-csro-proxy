# node-csro-proxy Project Roadmap
## MAIN TODO's
    - Reimplement connection writing to database & ip limit system;
    - Create administration interface + WEB GUI for connected instance management;
    - Fix webmall purchase packet result (currently hangs client side web mall interface);
    - Add titles system support;
    - Build EventManager for custom ingame events;
    - Connect more tables & figure out what we can do about all that;

## THINGS TO THINK ABOUT LATER
    - HWID implementation using the "electron-launcher" project;
    - Layer to add additional packet encryption
    - Packet documentation with schemas?