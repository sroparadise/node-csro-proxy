import config from '@config/Database';
import SQLAdapter from '@lib/SQLAdapter';
import routes from '@service/API_Routes';
import express from 'express';
import docFramework from 'express-oas-generator';
import crud, { sequelizeCrud } from 'express-sequelize-crud';

class WebServer {
    constructor(serverConfig, models) {
        this.serverConfig = serverConfig;
        this.adapter = new SQLAdapter({
            ...config,
            database: this.serverConfig.database
        }, models);
        this.routeKeys = routes[process.env.MODULE] ? Object.keys(routes[process.env.MODULE]) : false;
    }

    async run() {
        try {
            this.database = await this.adapter.instance();
            this.app = express();

            docFramework.handleResponses(this.app);

            this.app.set('database', this.database);

            const self = this;

            //LOCAL AUTH:
            this.app.use((req, res, next) => {
                req.database = self.database;
                next();
            });

            for (const name of Object.keys(this.database.models)) {
                this.app.use(crud(`/${name.toLowerCase()}`, sequelizeCrud(this.database.models[name])));
                console.log(`Model ${name} registered route: /${name.toLowerCase()}`);
            }

            if (this.routeKeys) {
                for (const key of this.routeKeys) {
                    const { config: routeConfig, controller } = routes[process.env.MODULE][key];

                    switch (routeConfig.method) {
                        case 'POST':
                            this.app.post(routeConfig.path, controller);
                        break;
                        default:

                        break;
                    }

                    console.log(`Custom route registered: ${routeConfig.path} (${key})`);
                }
            }

            this.app.use((err, req, res, next) => {
                res.status(404).json({ message: 'Not Found' });
            });

            docFramework.handleRequests(this.app, {});

            this.app.listen(this.serverConfig.port);
            console.log(`Webserver online: ${this.serverConfig.port} (${this.serverConfig.database})`);
        } catch (e) {
            throw new Error(e);
        }
    }
}

export default WebServer;