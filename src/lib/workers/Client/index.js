import { SilkroadSecurityJS as Security, stream } from 'silkroad-security';
import { Socket } from 'net';
import { createLogger, format, transports } from 'winston';
import api from '@lib/DataAPI';
import NodeCache from 'node-cache';
import moment from 'moment';
import path from 'path';
import * as ctrl from '@control';
import * as hook from './hooks';
import * as helper from './helpers';

const init_timestamp = moment().format('MM-DD-YYYY');
const { formatLog, translate } = helper;
const { config, info } = JSON.parse(process.argv[2]);
const filename = path.join(__dirname, '..', '..', '..', '..', 'logs', `${config.module.toLowerCase()}`, `${init_timestamp}_${info.ip}.log`);

const logger = createLogger({
    transports: [
        new transports.File({ filename }),
    ],
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new transports.Console({
        format: formatLog(format),
    }));
}

const memory = new NodeCache();
const socket = new Socket();
const security = {
    client: new Security(),
    remote: new Security()
};

const disconnect = async () => {
    const get_session = memory.get('session') || false;

    if (config.hooks && config.hooks.exit) await hook[config.hooks.exit](api, get_session);

    logger.log('info', 'disconnect', info);

    process.send({ type: 'disconnect' });
};

const middlewares = Object.keys(config.middlewares).reduce(
    (endpoints, mw) => ({
        ...endpoints,
        [mw]: Object.keys(config.middlewares[mw]).reduce(
            (handles, opcode) => ({
                ...handles,
                [opcode]: ctrl[config.middlewares[mw][opcode]],
            }),
            {}
        ),
    }),
    {}
);

const skipOpcodes = new Set([8194, 8193]);

async function handlePacket(sender, packet) {
    let target = (sender === 'client') ? 'remote' : 'client';

    security[sender].Recv(Buffer.from(packet).toJSON().data);

    const incomingStream = security[sender].GetPacketToRecv();

    for await (const incomingPacket of incomingStream) {
        

        if ((target === 'remote' && config.whitelist[incomingPacket.opcode]) || target == 'client') {

            const middleware = middlewares[sender][incomingPacket.opcode] || false;

            const runMiddleware = middleware ? await middleware(
                {
                    stream,
                    config,
                    api,
                    memory,
                    info,
                    logger,
                },
                incomingPacket,
                sender,
                target,
            ) : [
                {
                    packet: incomingPacket,
                    target,
                },
            ];

            for (const { packet: _packet, exit: _exit, target: _target } of runMiddleware) {
                
                if (_exit) return disconnect();

                if (_packet) {
                    // Log Client -> server packet
                    const { opcode, data, encrypted, massive } = _packet;

                    // dispatch the packet to target:
                    security[_target || target].Send(
                        opcode,
                        data,
                        encrypted || false,
                        massive || false,
                    );

                    // Write all client -> server to logfile // && new Set([_target, target]).has('remote')
                    if (!skipOpcodes.has(opcode) ) logger.log('info', 'packet_sent', {
                        timestamp: moment().format('MM-DD-YYYY HH:mm:ss'),
                        opcode: translate(opcode),
                        bytes: data.length,
                        encrypted: encrypted || false,
                        massive: massive || false,
                        target: _target || target,
                    });
                }

            }

        } else {
            // Log skipped packets (not whitelisted):
            const { opcode, data, encrypted, massive } = incomingPacket;

            if (!skipOpcodes.has(opcode)) logger.log('warn', 'packet_skipped', {
                timestamp: moment().format('MM-DD-YYYY HH:mm:ss'),
                sender,
                opcode: translate(opcode),
                bytes: data.length,
                encrypted,
                massive,
            });
        }
    }

    // Deliver the resulting packets:
    const outgoingStream = security[target].GetPacketToSend();

    outgoingStream.map(outgoingPacket => {
        switch (target) {
            case 'remote': socket.write(Buffer.from(outgoingPacket));
                break;
            default:
                process.send({
                    type: 'buffer',
                    data: outgoingPacket,
                });
                break;
        }
    });


};

logger.log('info', 'init', { 
    info, 
    //config,
});

security.client.GenerateHandshake(true, true, true);

// Connect to the remote server:
socket.connect({
    host: config.REMOTE.HOST,
    port: config.REMOTE.PORT,
    onread: {
        buffer: Buffer.alloc(8192),
    },
});

// Server -> Client
socket.on('data', async data => await handlePacket('remote', data));

// Client -> Server
process.on('message', async ({
    code,
    data,
}) => {
    switch (code) {
        case 1:
            await handlePacket('client', data);
            break;
        case 0: // disconnect from master
        default:
            disconnect();
            break;
    }
});

// Reasons for disconnect:
socket.on('error', () => disconnect());
socket.on('close', () => disconnect());
process.on('SIGTERM', () => disconnect());