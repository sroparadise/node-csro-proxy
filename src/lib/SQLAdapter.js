import { Sequelize, DataTypes } from 'sequelize';

const logging = process.env.NODE_ENV !== 'production' ? console.log : false;

class SQLAdapter {
    constructor({
        host,
        database,
        username,
        password,
        dialect = 'mssql',
        options = {},
    }, models) {
        this.models = models;
        this.database = new Sequelize(
            database,
            username,
            password,
            {
                port: 1433,
                host,
                dialect,
                dialectOptions: {
                    options,
                },
                logging,
            }
        );
    }

    async instance() {
        try {
            await this.database.authenticate();
            for (const key of Object.keys(this.models)) {
                this.models[key](this.database, DataTypes);
            }
            return this.database;
        } catch (error) {
            throw `SQL Error: ${error.message}`;
        }
    }
}

export default SQLAdapter;