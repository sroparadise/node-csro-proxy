import dotenv from 'dotenv';

dotenv.config();

const { NODE_ENV } = process.env;
const isDevelopment = (NODE_ENV !== 'production');

export default {
    proxy: {
        database: `MGUARD`,
        host: `http://127.0.0.1`,
        port: isDevelopment ? 10801 : 10991
    },
    account: {
        database: `SRO_R_ACCOUNT`,
        host: `http://127.0.0.1`,
        port: isDevelopment ? 10802 : 10992
    },
    shard: {
        database: `SRO_R_SHARD`,
        host: `http://127.0.0.1`,
        port: isDevelopment ? 10803 : 10993
    },
    addon: {
        database: `SR_ADDON_DB`,
        host: `http://127.0.0.1`,
        port: isDevelopment ? 10804 : 10994
    }
}