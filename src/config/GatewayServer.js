import dotenv from 'dotenv';

dotenv.config();

const {
    MODULE,
    IP_LIMIT,
    HWID_LIMIT,
    BLOCKED_COUNTRIES,
    BIND_IP,
    BIND_PORT,
    REMOTE_IP,
    REMOTE_PORT,
    NODE_ENV,
} = process.env;

const isDevelopment = (NODE_ENV !== 'production');

export default {
    module: MODULE || 'GatewayServer',
    LIMITS: {
        IP: IP_LIMIT || 20,
        HWID: HWID_LIMIT || 3,
    },
    BANNED_COUNTRY_CODES: BLOCKED_COUNTRIES ? BLOCKED_COUNTRIES.split(',') : [],
    LOCAL: {
        HOST: BIND_IP || '0.0.0.0',
        PORT: BIND_PORT || (isDevelopment ? 1337 : 1453),
    },
    REMOTE: {
        HOST: REMOTE_IP || '148.251.195.215',
        PORT: REMOTE_PORT || (isDevelopment ? 15779 : 1453),
    },
    middlewares: {
        client: {},
        remote: {
            0xA102: 'RedirectAgentServer',
        },
    },
    whitelist: {        
        0x2002: 'GLOBAL_PING',
        0x9000: 'GLOBAL_HANDSHAKE_ACCEPT',
        0x6100: 'PATCH_REQUEST',
        0x6101: 'SHARD_LIST_REQUEST',
        0x6102: 'LOGIN_REQUEST',
        0x6104: 'NOTICE_REQUEST',
        0x6106: 'SHARD_LIST_PING_REQUEST',
        0xCAFE: 'HWID_REGISTRATION'
    },
};