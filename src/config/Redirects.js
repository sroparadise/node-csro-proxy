import dotenv from 'dotenv';

dotenv.config();

const { NODE_ENV, REDIRECT_AGENT_IP, REDIRECT_AGENT_PORT } = process.env;
const isDevelopment = (NODE_ENV !== 'production');

export default {
    // AgentServer
    ':12': { // <-- IP FROM
        host: REDIRECT_AGENT_IP || '138.201.58.79', // <-- IP TO
        port: REDIRECT_AGENT_PORT || (isDevelopment ? 8082 : 8002), // <-- PORT TO
    },
    ':13': { // <-- IP FROM
        host: REDIRECT_AGENT_IP || '138.201.58.79', // <-- IP TO
        port: REDIRECT_AGENT_PORT || (isDevelopment ? 8082 : 8002), // <-- PORT TO
    },
    ':15': { // <-- IP FROM
        host: REDIRECT_AGENT_IP || '138.201.58.79', // <-- IP TO
        port: REDIRECT_AGENT_PORT || (isDevelopment ? 8082 : 8002), // <-- PORT TO
    },
    ':16': { // <-- IP FROM
        host: REDIRECT_AGENT_IP || '138.201.58.79', // <-- IP TO
        port: REDIRECT_AGENT_PORT || (isDevelopment ? 8082 : 8002), // <-- PORT TO
    },
    ':17': { // <-- IP FROM
        host: REDIRECT_AGENT_IP || '138.201.58.79', // <-- IP TO
        port: REDIRECT_AGENT_PORT || (isDevelopment ? 8082 : 8002), // <-- PORT TO
    },
    // USE FOR MULTI AGENT, CAN USE SIMILAR PATTERN FOR MORE e.g. AGENT3_
    // ':14': { // <-- AGENT2 IP FROM
    //     host: REDIRECT_AGENT2_IP || '138.201.58.79', // <-- IP TO
    //     port: REDIRECT_AGENT2_PORT || 8003, // <-- PORT TO
    // },

    // DownloadServer
    // '148.251.195.215:16002': { // <-- IP FROM
    //     host: REDIRECT_DOWNLOAD_IP || '127.0.0.1', // <-- IP TO
    //     port: REDIRECT_DOWNLOAD_PORT || 8003, // <-- PORT TO
    // },
};