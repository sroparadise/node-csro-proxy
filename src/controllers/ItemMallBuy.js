async function ItemMallBuy(
    {
        stream: {
            reader,
            writer,
        },
        memory,
        logger,
        api: {
            account,
        }
    },
    packet,
) {
    const [
        CharName16,
        JID,
    ] = [
            memory.get('CharName16'),
            memory.get('UserJID'),
        ];

    const read = new reader(packet.data);
    const shopType = read.uint8();

    if (shopType === 24) {
        const arg1 = read.uint16();
        const arg2 = read.uint8();
        const arg3 = read.uint8();
        const arg4 = read.uint8()
        const package_code = read.string();
        const count = read.uint16();

        const {
            data: {
                result: purchase_result
            }
        } = await account.post(`/mall-buy`, {
            CharName16,
            package_code,
            count,
        });

        logger.log('info', 'purchase_mall', {
            CharName16,
            package_code,
            count,
            purchase_result,
            args: {
                arg1,
                arg2,
                arg3,
                arg4,
            },
        });

        const {
            data: [
                {
                    silk_own,
                    silk_gift,
                    silk_point,
                },
            ]
        } = await account.get(`/sk_silk`, {
            params: {
                sort: JSON.stringify(['JID']),
                filter: JSON.stringify({
                    JID,
                }),
            },
        });


        const silk_update = () => {
            const write = new writer();
            for (const amount of [
                silk_own,
                silk_gift,
                silk_point,
            ]) {
                write.uint32(amount);
            }
            
            return {
                packet: {
                    opcode: 0x3153,
                    data: write.toData(),
                },
                target: 'client',
            };
        };

        const inventory_update = () => {
            const write = new writer();
            write.uint8(1);
            write.uint8(24);
            write.uint16(arg1);
            write.uint8(arg2);
            write.uint8(arg3);
            write.uint8(arg4);
            write.uint8(1);
            write.uint8(purchase_result);
            write.uint16(1);
            write.uint32(0);
            return {
                packet: {
                    opcode: 0xB034,
                    data: write.toData(),
                },
                target: 'client',
            };
        };

        const collect_item_update = () => {
            const write = new writer();
            write.uint8(1);
            write.uint8(7);
            write.uint8(purchase_result);

            return {
                packet: {
                    opcode: 0xB034,
                    data: write.toData(),
                },
                target: 'client',
            };
        };

        return [
            inventory_update(),
            collect_item_update(),
            silk_update(),
        ];
    } else {
        return [
            { packet },
        ];
    }
}

export default ItemMallBuy;
