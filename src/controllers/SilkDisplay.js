async function SilkDisplay(
    {
        stream: {
            writer,
        },
        api: {
            account,
        },
        memory,
        logger,
    },
    packet,
) {
    const write = new writer();
    const JID = memory.get('UserJID');
   
    const {
        data: [
            {
                silk_own,
                silk_gift,
                silk_point,
            },
        ]
    } = await account.get(`/sk_silk`, {
        params: {
            sort: JSON.stringify(['JID']),
            filter: JSON.stringify({
                JID,
            }),
        }
    });

    logger.log('info', 'sent_silk_info', {
        silk_own,
        silk_gift,
        silk_point,
    });

    for (const amount of [
        silk_own,
        silk_gift,
        silk_point,
    ]) {
        write.uint32(amount);
    }

    return [
        {
            packet: {
                opcode: 0x3153,
                data: write.toData(),
            },
            target: 'client',
        },
        {
            packet,
            target: 'remote',
        },
    ];
}

export default SilkDisplay;