async function CharacterWalk(
    {
        memory,
        stream: {
            writer,
        },
    },
    packet,
) {
    const isAwayStatus = memory.get('isAwayStatus') || false;

    if (isAwayStatus) {
        memory.set('isAwayStatus', false);
        memory.set('lastActivityTime', new Date());

        const update_afk = () => {
            const write = new writer();
            write.uint8(0);

            return {
                packet: {
                    opcode: 0x7402,
                    data: write.toData(),
                },
                target: 'remote',
            };
        };

        return [
            {
                packet,
                target: 'remote',
            },
            update_afk(),
        ];
    }

    return [{ packet }];
}

export default CharacterWalk;