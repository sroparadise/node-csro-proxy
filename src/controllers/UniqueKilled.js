async function UniqueKilled(
    {
        logger,
        config: {
            UNIQUES,
        },
        stream: {
            reader,
            writer,
        },
        memory,
        api: {
            account,
            shard,
        }
    },
    packet,
) {
    try {
        const current_charname = memory.get('CharName16'); //whoami
        const read = new reader(packet.data);

        if (read.uint8() == 6) {
            const uniqueId = read.uint16();
            const arg1 = read.uint8();
            const arg2 = read.uint16();
            const killer_name = read.string();

            if (killer_name == current_charname) {
                const unique_config = UNIQUES[uniqueId] || false;
                const UserJID = memory.get('UserJID');
                const CharID = memory.get('CharID');

                // Retrieve actual char information
                const {
                    data: [
                        {
                            CurLevel,
                        }
                    ]
                } = await shard.get(`/_char`, {
                    params: {
                        sort: JSON.stringify(['CharID']),
                        filter: JSON.stringify({
                            CharID,
                        }),
                    }
                });

                logger.log('info', 'killed_unique', {
                    UserJID,
                    killer_name,
                    CurLevel,
                    reward_config: unique_config,
                    unique_id: uniqueId,
                });
                // we may wanna dispatch this event to discord here later

                if (unique_config && CurLevel <= unique_config.cap) {
                    // Dispatch silk reward
                    await account.post(`/reward-silk`, {
                        UserJID,
                        CharID,
                        amount: unique_config.reward,
                    });

                    // Send a notice to user after the unique kill:
                    const write = new writer();
                    write.uint8(7);
                    write.string(`Great! You got ${unique_config.reward} silk for killing [${unique_config.name}]`);

                    // packets go in order as specified (one object per packet):
                    return [
                        { // return original packet s->c
                            packet, 
                            target: 'client',
                        },
                        {
                            packet: { // extra packet proxy->c
                                opcode: 0x3026,
                                data: write.toData(),
                            },
                            target: 'client',
                        },
                    ];
                }
            }
        }

        return [{ packet, target: 'client' }];
    } catch (e) {
        return [{ packet }];
    }
}

export default UniqueKilled;