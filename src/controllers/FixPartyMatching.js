async function FixPartyMatching(
    {
        logger,
        config: {
            maxPartyMatchingPacketSize,
        },
    },
    packet,
) {
    if (!packet.data.length || packet.data.length <= maxPartyMatchingPacketSize) {
        return [
            {
                packet,
            },
        ];
    } else {
        logger.log(`error`, `attempted_to_hack`, {
            packet,
        });
        return [
            {
                exit: true,
            },
        ]
    }
}

export default FixPartyMatching;
