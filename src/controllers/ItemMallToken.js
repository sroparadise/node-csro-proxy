async function ItemMallToken(
    {
        stream: {
            writer,
        },
        api: {
            account,
        },
        memory,
        logger,
    },
    packet,
) {
    
    const username = memory.get('StrUserID');

    const {
        data: {
            JID,
            token,
        },
    } = await account.post(`/mall-token`, {
        StrUserID: username,
    });

    logger.log('info', 'send_mall_token', {
        username,
        JID,
        token,
    });

    const update_token = () => {
        const write = new writer();
        write.uint8(1);
        write.uint32(JID);
        write.string(token);
        return {
            packet: {
                opcode: 0xB566,
                encrypted: true,
                data: write.toData(),
            },
            target: 'client',
        };
    };
    
    return [
        update_token(),
        // {
        //     packet,
        //     target: 'remote',
        // }
    ];
}

export default ItemMallToken;
