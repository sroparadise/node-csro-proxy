async function SelectCharacter(
    {
        stream: {
            reader,
        },
        logger,
        api: {
            shard,
            account,
        },
        memory,
    },
    packet,
) {
    const read = new reader(packet.data);
    const name = read.string();

    const {
        data: [
            {
                CharID,
                CharName16,
            }
        ]
    } = await shard.get(`/_char`, {
        params: {
            sort: JSON.stringify(['CharID']),
            filter: JSON.stringify({
                CharName16: name,
            }),
        }
    });

    const {
        data: [
            {
                UserJID,
            }
        ]
    } = await shard.get(`/_user`, {
        params: {
            sort: JSON.stringify(['CharID']),
            filter: JSON.stringify({
                CharID,
            }),
        }
    });

    const {
        data: [
            {
                sec_primary,
                sec_content,
                StrUserID,
            },
        ]
    } = await account.get(`/tb_user`, {
        params: {
            sort: JSON.stringify(['JID']),
            filter: JSON.stringify({
                JID: UserJID,
            }),
        }
    });
    
    const memory_init = {
        CharName16,
        CharID,
        UserJID,
        sec_primary,
        sec_content,
        StrUserID,
        lastActivityTime: new Date(),
        isAwayStatus: true,
    };

    Object.keys(memory_init).map(key => memory.set(key, memory_init[key]));

    logger.log('info', 'join_game', memory_init);
    
    return [
        { packet },
    ];
}

export default SelectCharacter;