import { sample } from 'lodash';

async function WeatherPacket(
    {
        config,
        stream,
        logger,
    },
    packet,
) {
    const { writer } = stream;
    const { type, intensity } = config.weather;

    const selectedWeather = sample(type);
    const selectedIntensity = sample(intensity);

    logger.log('info', 'send_weather', {
        type: selectedWeather,
        intensity: selectedIntensity,
    });

    const write = new writer();

    write.uint8(selectedWeather);
    write.uint8(selectedIntensity);

    return [
        {
            packet: {
                ...packet,
                data: write.toData(),
            },
        },
    ];
}

export default WeatherPacket;
