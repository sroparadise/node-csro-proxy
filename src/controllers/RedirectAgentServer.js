import redirects from '@config/redirects';

async function RedirectAgentServer(
    {
        stream,
        logger,
    },
    packet,
) {
    const { writer, reader } = stream;

    const read = new reader(packet.data);
    const status = read.uint8();

    if (status == 1) {
        const token = read.uint16();
        const host = read.string();
        const port = read.uint16();
        const redirect = redirects[`${host}:${port}`];

        logger.log('info', 'agent_redirect', {
            token,
            host,
            port,
            redirect,
        });

        const write = new writer();

        write.uint8(status);
        write.uint32(token);
      
        write.string(redirect.host);
        write.uint16(redirect.port);

        return [
            {
                packet: {
                    ...packet,
                    data: write.toData(),
                },
            }
        ];
    }

    return [
        { packet },
    ];
}

export default RedirectAgentServer;
