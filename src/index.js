import dotenv from 'dotenv';
import findProcess from 'find-process';
import * as service from '@service';

dotenv.config();

const index = process.env.MODULE || false;
const isDev = process.env.NODE_ENV == 'development';

const exit = () => {
    console.log(`[${index}]->exit`);
    process.exit(0);
};

const init = () => {
    try {
        index && new service[index]().run();
    } catch (error) {
        console.log(`[${index}]->(error)->${error}`);
    }
};

process.on('unhandledRejection', (error, promise) => {
    if (isDev) {
        console.log(`[${index}]->(UnhandledPromiseRejection)->${error}`);
        exit();
    } else {
        promise.catch();
    }
});

process.on('uncaughtException', async (error) => {
    if (isDev) console.log(`[${index}]->(UncaughtException)->${error}`);
    
    if (isDev && error.code && error.code == 'EADDRINUSE') {
        const [dead] = await findProcess('port', error.port);

        if (dead) {
            console.log(`Port not open [${error.port}]->reinit`);
            process.kill(dead.pid);
            init();
        } else {
            init();
        }
    }
});

init();