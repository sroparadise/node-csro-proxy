export default (db, types) => db.define('PvpPoint', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    KillerCharID: {
      type: types.TEXT,
      allowNull: true
    },
    KilledCharID: {
      type: types.TEXT,
      allowNull: true
    },
    RewardPoint: {
      type: types.INTEGER,
      allowNull: true
    },
    Desc: {
      type: types.TEXT,
      allowNull: true
    },
    JobTip: {
      type: types.INTEGER,
      allowNull: true
    },
    Time: {
      type: types.TEXT,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'PvpPoint',
    schema: 'dbo',
    timestamps: false
});
