export default (db, types) => db.define('__StatisticsGoldIncrementData__', {
    BeginDate: {
      type: types.DATE,
      allowNull: false,
      primaryKey: true
    },
    EndDate: {
      type: types.DATE,
      allowNull: true
    },
    Paid: {
      type: types.BIGINT,
      allowNull: true
    },
    Income: {
      type: types.BIGINT,
      allowNull: true
    },
    HunterProfit: {
      type: types.BIGINT,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__StatisticsGoldIncrementData__',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK____StatisticsGold__7A9C383C",
        unique: true,
        fields: [
          { name: "BeginDate" },
        ]
      },
    ]
});
