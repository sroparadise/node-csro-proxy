export default (db, types) => db.define('PvpPoint_Total', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CharID: {
      type: types.TEXT,
      allowNull: true
    },
    CharName16: {
      type: types.TEXT,
      allowNull: true
    },
    TotalPoint: {
      type: types.INTEGER,
      allowNull: true
    },
    JobTip: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'PvpPoint_Total',
    schema: 'dbo',
    timestamps: false
});
