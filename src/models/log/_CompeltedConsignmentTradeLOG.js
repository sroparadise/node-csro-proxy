export default (db, types) => db.define('_CompeltedConsignmentTradeLOG', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    EventTime: {
      type: types.DATE,
      allowNull: false
    },
    ProgressID: {
      type: types.INTEGER,
      allowNull: false
    },
    JobType: {
      type: types.TINYINT,
      allowNull: false
    },
    Tiredness: {
      type: types.INTEGER,
      allowNull: false
    },
    TotalCount: {
      type: types.INTEGER,
      allowNull: false
    },
    RemainCount: {
      type: types.INTEGER,
      allowNull: false
    },
    Magnification: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_CompeltedConsignmentTradeLOG',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__CompeltedConsignmentTradeLOG__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
