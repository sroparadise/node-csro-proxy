export default (db, types) => db.define('_ConsignmentTrade_PrevInvestCharLog', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    EventTime: {
      type: types.DATE,
      allowNull: false
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    ProgressID: {
      type: types.INTEGER,
      allowNull: false
    },
    RefObjID: {
      type: types.INTEGER,
      allowNull: false
    },
    InvestCount: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_ConsignmentTrade_PrevInvestCharLog',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK___ConsignmentTrade_PrevInvestCharLog__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
