export default (db, types) => db.define('_Online', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CharID: {
      type: types.STRING(200),
      allowNull: true
    },
    CharName16: {
      type: types.STRING(200),
      allowNull: true
    },
    CharSkin: {
      type: types.STRING(200),
      allowNull: true
    },
    Status: {
      type: types.STRING(200),
      allowNull: true
    },
    LastLogin: {
      type: types.STRING(200),
      allowNull: true
    },
    LastLogout: {
      type: types.STRING(200),
      allowNull: true
    },
    ToplamDk: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_Online',
    schema: 'dbo',
    timestamps: false
});
