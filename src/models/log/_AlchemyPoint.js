export default (db, types) => db.define('_AlchemyPoint', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CharID: {
      type: types.STRING(100),
      allowNull: true
    },
    CharName16: {
      type: types.STRING(100),
      allowNull: true
    },
    Point: {
      type: types.INTEGER,
      allowNull: true
    },
    MaxPlus: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_AlchemyPoint',
    schema: 'dbo',
    timestamps: false
});
