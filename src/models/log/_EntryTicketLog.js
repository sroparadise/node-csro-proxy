export default (db, types) => db.define('_EntryTicketLog', {
    LogType: {
      type: types.INTEGER,
      allowNull: false
    },
    TicketType: {
      type: types.INTEGER,
      allowNull: false
    },
    EventID: {
      type: types.INTEGER,
      allowNull: false
    },
    JID: {
      type: types.INTEGER,
      allowNull: false
    },
    CharName: {
      type: types.STRING(64),
      allowNull: false
    },
    ItemCodeName: {
      type: types.STRING(128),
      allowNull: false
    },
    Desc: {
      type: types.STRING(128),
      allowNull: false
    },
    EntryDate: {
      type: "SMALLDATETIME",
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_EntryTicketLog',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__EntryTicketLog_EventID",
        fields: [
          { name: "EventID" },
        ]
      },
      {
        name: "IX__EntryTicketLog_JID",
        fields: [
          { name: "JID" },
        ]
      },
    ]
});
