export default (db, types) => db.define('_LogWebCashItem', {
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    EventID: {
      type: types.INTEGER,
      allowNull: false
    },
    IDX: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    SubIDX: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    Section: {
      type: types.INTEGER,
      allowNull: false
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: true
    },
    Count: {
      type: types.INTEGER,
      allowNull: true
    },
    RecvDate: {
      type: "SMALLDATETIME",
      allowNull: false
    },
    Serial64: {
      type: types.BIGINT,
      allowNull: false
    },
    strDesc: {
      type: types.STRING(128),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_LogWebCashItem',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__LogWebCashItem_1",
        unique: true,
        fields: [
          { name: "IDX" },
          { name: "SubIDX" },
        ]
      },
    ]
});
