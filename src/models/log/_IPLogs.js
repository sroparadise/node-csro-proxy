export default (db, types) => db.define('_IPLogs', {
    'No.': {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: true
    },
    Charname: {
      type: types.TEXT,
      allowNull: true
    },
    IP: {
      type: types.TEXT,
      allowNull: true
    },
    Date: {
      type: types.DATE,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_IPLogs',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK___IPLogs__C7D1C6D02CC77177",
        unique: true,
        fields: [
          { name: "No." },
        ]
      },
    ]
});
