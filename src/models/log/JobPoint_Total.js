export default (db, types) => db.define('JobPoint_Total', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CharID: {
      type: types.TEXT,
      allowNull: true
    },
    CharName16: {
      type: types.TEXT,
      allowNull: true
    },
    TotalPoint: {
      type: types.INTEGER,
      allowNull: true
    },
    JobTip: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'JobPoint_Total',
    schema: 'dbo',
    timestamps: false
});
