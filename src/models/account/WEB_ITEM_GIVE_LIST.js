export default (db, types) => db.define('WEB_ITEM_GIVE_LIST', {
    idx: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    cp_jid: {
      type: types.INTEGER,
      allowNull: false
    },
    shard_id: {
      type: types.INTEGER,
      allowNull: false
    },
    character_id: {
      type: types.INTEGER,
      allowNull: true
    },
    character_lv: {
      type: types.INTEGER,
      allowNull: true
    },
    item_code_package: {
      type: types.STRING(128),
      allowNull: false
    },
    item_name_package: {
      type: types.STRING(64),
      allowNull: true
    },
    name_code_package: {
      type: types.STRING(128),
      allowNull: true
    },
    section: {
      type: types.INTEGER,
      allowNull: false
    },
    silk_own: {
      type: types.INTEGER,
      allowNull: false
    },
    silk_own_premium: {
      type: types.INTEGER,
      allowNull: false
    },
    silk_gift: {
      type: types.INTEGER,
      allowNull: false
    },
    silk_gift_premium: {
      type: types.INTEGER,
      allowNull: false
    },
    silk_point: {
      type: types.INTEGER,
      allowNull: false
    },
    message: {
      type: types.STRING(128),
      allowNull: false
    },
    reg_ip: {
      type: types.STRING(20),
      allowNull: false
    },
    reg_date: {
      type: types.DATE,
      allowNull: false
    },
    recieve_date: {
      type: types.DATE,
      allowNull: true
    },
    invoice_id: {
      type: types.INTEGER,
      allowNull: true
    },
    cp_invoice_id: {
      type: types.STRING(32),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'WEB_ITEM_GIVE_LIST',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_SK_ItemGiveList",
        unique: true,
        fields: [
          { name: "idx" },
        ]
      },
    ]
});
