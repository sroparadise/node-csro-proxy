export default (db, types) => db.define('WEB_PACKAGE_ITEM_DETAIL', {
    idx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    package_id: {
      type: types.INTEGER,
      allowNull: false
    },
    item_code: {
      type: types.STRING(128),
      allowNull: false
    },
    item_name: {
      type: types.STRING(64),
      allowNull: true
    },
    item_name_eng: {
      type: types.STRING(64),
      allowNull: true
    },
    item_quantity: {
      type: types.INTEGER,
      allowNull: false
    },
    ref_rent: {
      type: types.INTEGER,
      allowNull: true
    },
    item_description: {
      type: types.TEXT,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'WEB_PACKAGE_ITEM_DETAIL',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_PACKAGE_ITEM_DETAIL",
        unique: true,
        fields: [
          { name: "idx" },
        ]
      },
    ]
});
