export default (db, types) => db.define('WEB_PACKAGE_ITEM_POPUP_MESSAGE', {
    ID: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    service: {
      type: types.INTEGER,
      allowNull: false
    },
    package_id: {
      type: types.BIGINT,
      allowNull: false
    },
    item_name: {
      type: types.TEXT,
      allowNull: false
    },
    img_url: {
      type: types.TEXT,
      allowNull: false
    },
    start_time: {
      type: types.TEXT,
      allowNull: false
    },
    finish_time: {
      type: types.TEXT,
      allowNull: false
    },
    discount_rate: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WEB_PACKAGE_ITEM_POPUP_MESSAGE',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_PACKAGE_ITEM_POPUP_NOTICE",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
