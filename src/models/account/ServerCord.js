export default (db, types) => db.define('ServerCord', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ChildID: {
      type: types.SMALLINT,
      allowNull: false,
      references: {
        model: 'ServerBody',
        key: 'ID'
      }
    },
    ParentID: {
      type: types.SMALLINT,
      allowNull: false
    },
    BindType: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'ServerCord',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__ServerCo__3214EC278202D17A",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
