export default (db, types) => db.define('WEB_Payments', {
    ID: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    UserJID: {
      type: types.BIGINT,
      allowNull: false
    },
    provider: {
      type: types.STRING(50),
      allowNull: false
    },
    payment_ref_init: {
      type: types.STRING(256),
      allowNull: true
    },
    payment_ref_complete: {
      type: types.STRING(256),
      allowNull: true
    },
    payment_amount: {
      type: types.DECIMAL(18,2),
      allowNull: false
    },
    reward: {
      type: types.BIGINT,
      allowNull: false
    },
    created_at: {
      type: types.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('getdate')
    },
    updated_at: {
      type: types.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('getdate')
    }
  }, {
    sequelize: db,
    tableName: 'WEB_Payments',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_Payments",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
