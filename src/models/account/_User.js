export default (db, types) => db.define('_User', {
    nID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szUserID: {
      type: types.STRING(128),
      allowNull: false,
      unique: "IX__User"
    },
    szPassword: {
      type: types.STRING(32),
      allowNull: false
    },
    szName: {
      type: types.STRING(16),
      allowNull: false
    },
    SSNumber1: {
      type: types.STRING(32),
      allowNull: false
    },
    SSNumber2: {
      type: types.STRING(32),
      allowNull: false
    },
    szOrganization: {
      type: types.STRING(128),
      allowNull: false
    },
    szRoleDesc: {
      type: types.STRING(128),
      allowNull: false
    },
    nPrimarySecurityID: {
      type: types.TINYINT,
      allowNull: false
    },
    nContentSecurityID: {
      type: types.TINYINT,
      allowNull: false
    },
    GrantedSecurityIP: {
      type: types.STRING(16),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_User',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__User",
        unique: true,
        fields: [
          { name: "szUserID" },
        ]
      },
      {
        name: "PK__User",
        unique: true,
        fields: [
          { name: "nID" },
        ]
      },
    ]
});
