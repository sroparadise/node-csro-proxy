export default (db, types) => db.define('WEB_MALL_CATEGORY', {
    shop_no: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    shop_name: {
      type: types.STRING(50),
      allowNull: true
    },
    shop_name_tr: {
      type: types.STRING(50),
      allowNull: true
    },
    shop_name_eg: {
      type: types.STRING(50),
      allowNull: true
    },
    shop_name_es: {
      type: types.STRING(50),
      allowNull: true
    },
    shop_name_de: {
      type: types.STRING(50),
      allowNull: true
    },
    shop_order: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WEB_MALL_CATEGORY',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_MALL_CATEGORY",
        unique: true,
        fields: [
          { name: "shop_no" },
        ]
      },
    ]
});
