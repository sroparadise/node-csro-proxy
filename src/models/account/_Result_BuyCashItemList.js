export default (db, types) => db.define('_Result_BuyCashItemList', {
    ShardID: {
      type: types.INTEGER,
      allowNull: false
    },
    idx: {
      type: types.BIGINT,
      allowNull: false
    },
    cp_jid: {
      type: types.INTEGER,
      allowNull: false
    },
    section: {
      type: types.INTEGER,
      allowNull: false
    },
    name_code_package: {
      type: types.STRING(128),
      allowNull: true
    },
    SubIDx: {
      type: types.BIGINT,
      allowNull: false
    },
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    item_quantity: {
      type: types.INTEGER,
      allowNull: false
    },
    reg_date: {
      type: types.DATE,
      allowNull: false
    },
    rent_type: {
      type: types.INTEGER,
      allowNull: true
    },
    can_delete: {
      type: types.SMALLINT,
      allowNull: true
    },
    can_recharge: {
      type: types.SMALLINT,
      allowNull: true
    },
    date_begin: {
      type: "SMALLDATETIME",
      allowNull: true
    },
    date_end: {
      type: "SMALLDATETIME",
      allowNull: true
    },
    time_count: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_Result_BuyCashItemList',
    schema: 'dbo',
    timestamps: false
});
