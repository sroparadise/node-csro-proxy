export default (db, types) => db.define('FarmContent', {
    FarmID: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Farm',
        key: 'ID'
      }
    },
    ContentID: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Content',
        key: 'ID'
      }
    }
  }, {
    sequelize: db,
    tableName: 'FarmContent',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__FarmCont__1FEBC01E1A6A80B4",
        unique: true,
        fields: [
          { name: "FarmID" },
          { name: "ContentID" },
        ]
      },
    ]
});
