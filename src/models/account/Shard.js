export default (db, types) => db.define('Shard', {
    ID: {
      type: types.SMALLINT,
      allowNull: false,
      primaryKey: true
    },
    FarmID: {
      type: types.TINYINT,
      allowNull: true,
      references: {
        model: 'Farm',
        key: 'ID'
      }
    },
    ContentID: {
      type: types.TINYINT,
      allowNull: true,
      references: {
        model: 'Content',
        key: 'ID'
      }
    },
    Name: {
      type: types.STRING(32),
      allowNull: false
    },
    DBConfig: {
      type: types.STRING(256),
      allowNull: true
    },
    LogDBConfig: {
      type: types.STRING(256),
      allowNull: true
    },
    MaxUser: {
      type: types.SMALLINT,
      allowNull: false
    },
    ShardManagerID: {
      type: types.SMALLINT,
      allowNull: true,
      references: {
        model: 'ServerBody',
        key: 'ID'
      }
    }
  }, {
    sequelize: db,
    tableName: 'Shard',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__Shard__3214EC270B93CBD2",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
