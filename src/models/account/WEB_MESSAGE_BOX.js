export default (db, types) => db.define('WEB_MESSAGE_BOX', {
    idx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    from_cp_jid: {
      type: types.INTEGER,
      allowNull: true
    },
    from_user_id: {
      type: types.STRING(50),
      allowNull: true
    },
    from_shard: {
      type: types.STRING(50),
      allowNull: true
    },
    from_character: {
      type: types.STRING(50),
      allowNull: true
    },
    to_cp_jid: {
      type: types.INTEGER,
      allowNull: true
    },
    subject: {
      type: types.STRING(50),
      allowNull: true
    },
    contents: {
      type: types.STRING(1000),
      allowNull: true
    },
    is_del: {
      type: types.TINYINT,
      allowNull: true
    },
    open_date: {
      type: types.DATE,
      allowNull: true
    },
    reg_date: {
      type: types.DATE,
      allowNull: false
    },
    silk: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'WEB_MESSAGE_BOX',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_MESSAGE_BOX",
        unique: true,
        fields: [
          { name: "idx" },
        ]
      },
    ]
});
