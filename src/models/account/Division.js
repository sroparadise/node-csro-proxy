export default (db, types) => db.define('Division', {
    ID: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    Name: {
      type: types.STRING(32),
      allowNull: false
    },
    DBConfig: {
      type: types.STRING(256),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'Division',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__Division__3214EC271A898B6D",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
