export default (db, types) => db.define('WEB_Vote', {
    ID: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    UserJID: {
      type: types.BIGINT,
      allowNull: false
    },
    site_id: {
      type: types.STRING(50),
      allowNull: false
    },
    ip: {
      type: types.STRING(256),
      allowNull: false
    },
    reward: {
      type: types.INTEGER,
      allowNull: false
    },
    timestamp: {
      type: types.BIGINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WEB_Vote',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_Vote",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
