export default (db, types) => db.define('WEB_PACKAGE_ITEM_LANG', {
    package_id: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    package_name: {
      type: types.STRING(64),
      allowNull: true
    },
    us_explain: {
      type: types.STRING(2048),
      allowNull: true
    },
    us_use_method: {
      type: types.STRING(255),
      allowNull: true
    },
    us_use_restriction: {
      type: types.STRING(255),
      allowNull: true
    },
    tr_explain: {
      type: types.STRING(2048),
      allowNull: true
    },
    tr_use_method: {
      type: types.STRING(255),
      allowNull: true
    },
    tr_use_restriction: {
      type: types.STRING(255),
      allowNull: true
    },
    eg_explain: {
      type: types.STRING(2048),
      allowNull: true
    },
    eg_use_method: {
      type: types.STRING(255),
      allowNull: true
    },
    eg_use_restriction: {
      type: types.STRING(255),
      allowNull: true
    },
    es_explain: {
      type: types.STRING(2048),
      allowNull: true
    },
    es_use_method: {
      type: types.STRING(255),
      allowNull: true
    },
    es_use_restriction: {
      type: types.STRING(255),
      allowNull: true
    },
    de_explain: {
      type: types.STRING(2048),
      allowNull: true
    },
    de_use_method: {
      type: types.STRING(255),
      allowNull: true
    },
    de_use_restriction: {
      type: types.STRING(255),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'WEB_PACKAGE_ITEM_LANG',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_PACKAGE_ITEM_LANG",
        unique: true,
        fields: [
          { name: "package_id" },
        ]
      },
    ]
});
