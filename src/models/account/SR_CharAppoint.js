export default (db, types) => db.define('SR_CharAppoint', {
    UserJID: {
      type: types.INTEGER,
      allowNull: false
    },
    ShardID: {
      type: types.INTEGER,
      allowNull: false
    },
    CharID: {
      type: types.STRING(64),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'SR_CharAppoint',
    schema: 'dbo',
    timestamps: false
});
