export default (db, types) => db.define('ServerMachine', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    DivisionID: {
      type: types.TINYINT,
      allowNull: true,
      references: {
        model: 'Division',
        key: 'ID'
      }
    },
    Name: {
      type: types.STRING(32),
      allowNull: false
    },
    PublicIP: {
      type: types.STRING(16),
      allowNull: false
    },
    PrivateIP: {
      type: types.STRING(16),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'ServerMachine',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__ServerMa__3214EC276F13B687",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
