export default (db, types) => db.define('_IPLockDown', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      unique: "unique_ID"
    },
    JID: {
      type: types.INTEGER,
      allowNull: false,
      unique: "unique_JID"
    },
    IP: {
      type: types.STRING(128),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_IPLockDown',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK___IPLockD__3214EC27D4A9547F",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "unique_ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "unique_JID",
        unique: true,
        fields: [
          { name: "JID" },
        ]
      },
    ]
});
