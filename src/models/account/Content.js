export default (db, types) => db.define('Content', {
    ID: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    Name: {
      type: types.STRING(64),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'Content',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__Content__3214EC2719B8A7C8",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
