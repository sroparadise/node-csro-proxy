export default (db, types) => db.define('Module', {
    ID: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    Name: {
      type: types.STRING(64),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'Module',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__Module__3214EC27CCCF7591",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
