export default (db, types) => db.define('WEB_ITEM_CERTIFYKEY', {
    idx: {
      type: types.INTEGER,
      allowNull: false,
      autoIncrement: true
    },
    UserJID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Certifykey: {
      type: types.STRING(50),
      allowNull: false
    },
    ShardID: {
      type: types.INTEGER,
      allowNull: false
    },
    reg_date: {
      type: types.DATE,
      allowNull: false,
      primaryKey: true
    },
    CharLevel: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'WEB_ITEM_CERTIFYKEY',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_ITEM_CERTIFYKEY_reg_date_UserJID",
        unique: true,
        fields: [
          { name: "reg_date" },
          { name: "UserJID" },
        ]
      },
    ]
});
