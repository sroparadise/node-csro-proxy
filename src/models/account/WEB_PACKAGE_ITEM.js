export default (db, types) => db.define('WEB_PACKAGE_ITEM', {
    service: {
      type: types.TINYINT,
      allowNull: false
    },
    package_id: {
      type: types.INTEGER,
      allowNull: false,
      unique: "IX_WEB_PACKAGE_ITEM_package_id"
    },
    package_code: {
      type: types.STRING(128),
      allowNull: false,
      primaryKey: true
    },
    name_code: {
      type: types.STRING(128),
      allowNull: true
    },
    shop_type: {
      type: types.STRING(128),
      allowNull: true
    },
    shop_no: {
      type: types.INTEGER,
      allowNull: false
    },
    shop_no_sub: {
      type: types.INTEGER,
      allowNull: false
    },
    event_no: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    event_no_sub: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    silk_type: {
      type: types.TINYINT,
      allowNull: false
    },
    silk_price: {
      type: types.INTEGER,
      allowNull: false
    },
    silk_price_grow: {
      type: types.INTEGER,
      allowNull: true
    },
    silk_price_item: {
      type: types.INTEGER,
      allowNull: true
    },
    discount_rate: {
      type: types.INTEGER,
      allowNull: false
    },
    discount_rate_grow: {
      type: types.INTEGER,
      allowNull: true
    },
    discount_rate_item: {
      type: types.INTEGER,
      allowNull: true
    },
    origin_server: {
      type: types.INTEGER,
      allowNull: true
    },
    grow_server: {
      type: types.INTEGER,
      allowNull: true
    },
    item_server: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'WEB_PACKAGE_ITEM',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX_WEB_PACKAGE_ITEM_package_id",
        unique: true,
        fields: [
          { name: "package_id" },
        ]
      },
      {
        name: "PK_WEB_PACKAGE_ITEM_LIST",
        unique: true,
        fields: [
          { name: "package_code" },
        ]
      },
    ]
});
