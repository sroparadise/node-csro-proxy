export default (db, types) => db.define('WEB_FREE_CHARGE_LOG', {
    idx: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    cp_jid: {
      type: types.BIGINT,
      allowNull: false
    },
    oder_no: {
      type: types.STRING(50),
      allowNull: false
    },
    add_silk: {
      type: types.INTEGER,
      allowNull: false
    },
    add_silk_real: {
      type: types.INTEGER,
      allowNull: false
    },
    total_silk: {
      type: types.INTEGER,
      allowNull: false
    },
    oid: {
      type: types.STRING(100),
      allowNull: true
    },
    sig: {
      type: types.STRING(100),
      allowNull: true
    },
    server_ip: {
      type: types.STRING(50),
      allowNull: false
    },
    reg_ip: {
      type: types.STRING(50),
      allowNull: false
    },
    reg_date: {
      type: types.DATE,
      allowNull: false
    },
    result_proc: {
      type: types.INTEGER,
      allowNull: false
    },
    result: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WEB_FREE_CHARGE_LOG',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_FREE_CHARGE_LOG",
        unique: true,
        fields: [
          { name: "idx" },
        ]
      },
    ]
});
