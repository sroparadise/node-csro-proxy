export default (db, types) => db.define('WEB_FREE_CHARGE', {
    cp_jid: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    total_silk: {
      type: types.INTEGER,
      allowNull: false
    },
    reg_ip: {
      type: types.STRING(50),
      allowNull: false
    },
    reg_date: {
      type: types.DATE,
      allowNull: false
    },
    location: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WEB_FREE_CHARGE',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_FREE_CHARGE",
        unique: true,
        fields: [
          { name: "cp_jid" },
        ]
      },
    ]
});
