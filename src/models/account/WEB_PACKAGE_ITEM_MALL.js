export default (db, types) => db.define('WEB_PACKAGE_ITEM_MALL', {
    package_id: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    item_order: {
      type: types.INTEGER,
      allowNull: false
    },
    is_best: {
      type: types.INTEGER,
      allowNull: false
    },
    is_new: {
      type: types.INTEGER,
      allowNull: false
    },
    is_sale: {
      type: types.INTEGER,
      allowNull: false
    },
    is_list: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    active: {
      type: types.TINYINT,
      allowNull: false
    },
    reg_date: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WEB_PACKAGE_ITEM_MALL',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_PACKAGE_ITEM_MALL",
        unique: true,
        fields: [
          { name: "package_id" },
        ]
      },
    ]
});
