export default (db, types) => db.define('WEB_ITEM_RENT_INFO', {
    idx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    subject: {
      type: types.STRING(300),
      allowNull: false
    },
    rent_type: {
      type: types.INTEGER,
      allowNull: false
    },
    can_delete: {
      type: types.SMALLINT,
      allowNull: false
    },
    can_recharge: {
      type: types.SMALLINT,
      allowNull: false
    },
    date_begin: {
      type: "SMALLDATETIME",
      allowNull: true
    },
    date_end: {
      type: "SMALLDATETIME",
      allowNull: true
    },
    time_count: {
      type: types.INTEGER,
      allowNull: true
    },
    reg_date: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WEB_ITEM_RENT_INFO',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_ITEM_RENT_INFO",
        unique: true,
        fields: [
          { name: "idx" },
        ]
      },
    ]
});
