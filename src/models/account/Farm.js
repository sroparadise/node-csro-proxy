export default (db, types) => db.define('Farm', {
    ID: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    DivisionID: {
      type: types.TINYINT,
      allowNull: true,
      references: {
        model: 'Division',
        key: 'ID'
      }
    },
    Name: {
      type: types.STRING(32),
      allowNull: false
    },
    DBConfig: {
      type: types.STRING(256),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'Farm',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__Farm__3214EC278DCA4064",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
