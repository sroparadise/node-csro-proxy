export default (db, types) => db.define('WEB_ITEM_GIVE_LIST_DETAIL', {
    idx: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    ref_idx: {
      type: types.BIGINT,
      allowNull: false
    },
    item_code: {
      type: types.STRING(128),
      allowNull: false
    },
    item_name: {
      type: types.STRING(64),
      allowNull: true
    },
    item_quantity: {
      type: types.INTEGER,
      allowNull: false
    },
    ref_rent: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'WEB_ITEM_GIVE_LIST_DETAIL',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_ITEM_GIVE_LIST_DETAIL",
        unique: true,
        fields: [
          { name: "idx" },
        ]
      },
    ]
});
