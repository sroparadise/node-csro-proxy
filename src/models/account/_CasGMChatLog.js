export default (db, types) => db.define('_CasGMChatLog', {
    nSerial: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szGM: {
      type: types.STRING(20),
      allowNull: false
    },
    wShardID: {
      type: types.SMALLINT,
      allowNull: false
    },
    szCharName: {
      type: types.STRING(64),
      allowNull: false
    },
    nCasSerial: {
      type: types.INTEGER,
      allowNull: false
    },
    szGMChatLog: {
      type: types.STRING(4000),
      allowNull: true
    },
    dWritten: {
      type: types.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('getdate')
    }
  }, {
    sequelize: db,
    tableName: '_CasGMChatLog',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK___CasGMChatLog__61316BF4",
        unique: true,
        fields: [
          { name: "nSerial" },
        ]
      },
    ]
});
