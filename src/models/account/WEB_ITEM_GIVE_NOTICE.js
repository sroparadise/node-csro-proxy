export default (db, types) => db.define('WEB_ITEM_GIVE_NOTICE', {
    idx: {
      type: types.BIGINT,
      allowNull: false,
      autoIncrement: true
    },
    ref_idx: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    cp_jid: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WEB_ITEM_GIVE_NOTICE',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_SK_ItemGiveNotice",
        unique: true,
        fields: [
          { name: "ref_idx" },
        ]
      },
    ]
});
