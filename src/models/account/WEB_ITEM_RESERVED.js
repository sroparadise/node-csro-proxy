export default (db, types) => db.define('WEB_ITEM_RESERVED', {
    idx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    userjid: {
      type: types.INTEGER,
      allowNull: false,
      unique: "IX_WEB_ITEM_RESERVED_1"
    },
    package_id: {
      type: types.INTEGER,
      allowNull: false,
      unique: "IX_WEB_ITEM_RESERVED_1"
    }
  }, {
    sequelize: db,
    tableName: 'WEB_ITEM_RESERVED',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX_WEB_ITEM_RESERVED_1",
        unique: true,
        fields: [
          { name: "userjid" },
          { name: "package_id" },
        ]
      },
      {
        name: "PK_WEB_ITEM_RESERVED",
        unique: true,
        fields: [
          { name: "idx" },
        ]
      },
    ]
});
