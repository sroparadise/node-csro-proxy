export default (db, types) => db.define('_ShardCurrentUser', {
    nID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nShardID: {
      type: types.INTEGER,
      allowNull: false
    },
    nUserCount: {
      type: types.INTEGER,
      allowNull: false
    },
    dLogDate: {
      type: "SMALLDATETIME",
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('getdate')
    }
  }, {
    sequelize: db,
    tableName: '_ShardCurrentUser',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__ShardCurrentUser",
        fields: [
          { name: "nShardID" },
        ]
      },
      {
        name: "PK___ShardCurrentUse__6D0D32F4",
        unique: true,
        fields: [
          { name: "nID" },
        ]
      },
    ]
});
