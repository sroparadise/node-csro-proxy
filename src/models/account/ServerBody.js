export default (db, types) => db.define('ServerBody', {
    ID: {
      type: types.SMALLINT,
      allowNull: false,
      primaryKey: true
    },
    DivisionID: {
      type: types.TINYINT,
      allowNull: true,
      references: {
        model: 'Division',
        key: 'ID'
      }
    },
    FarmID: {
      type: types.TINYINT,
      allowNull: true,
      references: {
        model: 'Farm',
        key: 'ID'
      }
    },
    ShardID: {
      type: types.SMALLINT,
      allowNull: true,
      references: {
        model: 'Shard',
        key: 'ID'
      }
    },
    MachineID: {
      type: types.INTEGER,
      allowNull: true,
      references: {
        model: 'ServerMachine',
        key: 'ID'
      }
    },
    ModuleID: {
      type: types.TINYINT,
      allowNull: true,
      references: {
        model: 'Module',
        key: 'ID'
      }
    },
    ModuleType: {
      type: types.TINYINT,
      allowNull: false,
      defaultValue: 0
    },
    CertifierID: {
      type: types.SMALLINT,
      allowNull: true
    },
    BindPort: {
      type: types.SMALLINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'ServerBody',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__ServerBo__3214EC27C75D694C",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
