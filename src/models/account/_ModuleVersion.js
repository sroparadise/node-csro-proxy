export default (db, types) => db.define('_ModuleVersion', {
    nID: {
      type: types.INTEGER,
      allowNull: false,
      autoIncrement: true
    },
    nDivisionID: {
      type: types.TINYINT,
      allowNull: false
    },
    nContentID: {
      type: types.TINYINT,
      allowNull: false
    },
    nModuleID: {
      type: types.TINYINT,
      allowNull: false
    },
    nVersion: {
      type: types.INTEGER,
      allowNull: false
    },
    szVersion: {
      type: types.STRING(64),
      allowNull: false
    },
    szDesc: {
      type: types.STRING(256),
      allowNull: false
    },
    nValid: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_ModuleVersion',
    schema: 'dbo',
    timestamps: false
});
