export default (db, types) => db.define('SK_SilkBuyList', {
    BuyNo: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    UserJID: {
      type: types.INTEGER,
      allowNull: false
    },
    Silk_Type: {
      type: types.TINYINT,
      allowNull: false
    },
    Silk_Reason: {
      type: types.TINYINT,
      allowNull: false
    },
    Silk_Offset: {
      type: types.INTEGER,
      allowNull: false
    },
    Silk_Remain: {
      type: types.INTEGER,
      allowNull: false
    },
    ID: {
      type: types.INTEGER,
      allowNull: false
    },
    BuyQuantity: {
      type: types.INTEGER,
      allowNull: false
    },
    YahooBuyNo: {
      type: types.STRING(16),
      allowNull: false
    },
    PayMethod: {
      type: types.STRING(2),
      allowNull: false
    },
    SubJID: {
      type: types.INTEGER,
      allowNull: false
    },
    srID: {
      type: types.STRING(128),
      allowNull: false
    },
    SlipPaper: {
      type: types.STRING(128),
      allowNull: false
    },
    MngID: {
      type: types.INTEGER,
      allowNull: false
    },
    IP: {
      type: types.STRING(16),
      allowNull: true
    },
    RegDate: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'SK_SilkBuyList',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "BuyList_IDX1",
        fields: [
          { name: "UserJID" },
        ]
      },
      {
        name: "BuyList_IDX2",
        fields: [
          { name: "YahooBuyNo" },
        ]
      },
      {
        name: "PK_SK_SilkBuyList",
        unique: true,
        fields: [
          { name: "BuyNo" },
        ]
      },
    ]
});
