export default (db, types) => db.define('WEB_MALL_CATEGORY_SUB', {
    ref_no: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    sub_no: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    sub_name: {
      type: types.STRING(50),
      allowNull: true
    },
    sub_name_tr: {
      type: types.STRING(50),
      allowNull: true
    },
    sub_name_eg: {
      type: types.STRING(50),
      allowNull: true
    },
    sub_name_es: {
      type: types.STRING(50),
      allowNull: true
    },
    sub_name_de: {
      type: types.STRING(50),
      allowNull: true
    },
    sub_order: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WEB_MALL_CATEGORY_SUB',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_WEB_MALL_CATEGORY_SUB",
        unique: true,
        fields: [
          { name: "ref_no" },
          { name: "sub_no" },
        ]
      },
    ]
});
