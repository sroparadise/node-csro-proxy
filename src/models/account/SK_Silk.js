export default (db, types) => db.define('SK_Silk', {
  JID: {
    type: types.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  silk_own: {
    type: types.INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  silk_gift: {
    type: types.INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  silk_point: {
    type: types.INTEGER,
    allowNull: false,
    defaultValue: 0
  }
}, {
  sequelize: db,
  tableName: 'SK_Silk',
  schema: 'dbo',
  timestamps: false,
  indexes: [
    {
      name: "PK__SK_Silk__1FCDBCEB",
      unique: true,
      fields: [
        { name: "JID" },
      ]
    },
  ]
});
