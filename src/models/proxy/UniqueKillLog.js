const UniqueKillLog = (db, types) => db.define('UniqueKillLog', {
    id: {
        type: types.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    char_id: {
        type: types.STRING,
        allowNull: false,
    },
    unique_id:  {
        type: types.INTEGER,
        allowNull: false,
    },
    is_awarded_silk:  {
        type: types.BOOLEAN,
        allowNull: false,
    },
    createdAt: types.DATE,
    updatedAt: types.DATE,
});

export default UniqueKillLog;