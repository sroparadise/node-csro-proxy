export default (db, types) => db.define('_InstantMobSpawnAtPosDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ServerID: {
      type: types.INTEGER,
      allowNull: false
    },
    WorldLayerID: {
      type: types.SMALLINT,
      allowNull: false
    },
    WorldID: {
      type: types.SMALLINT,
      allowNull: false
    },
    RegionID: {
      type: types.SMALLINT,
      allowNull: false
    },
    PosX: {
      type: types.FLOAT,
      allowNull: false
    },
    PosY: {
      type: types.FLOAT,
      allowNull: false
    },
    PosZ: {
      type: types.FLOAT,
      allowNull: false
    },
    MobType: {
      type: types.TINYINT,
      allowNull: false
    },
    Range: {
      type: types.FLOAT,
      allowNull: false
    },
    RefObjID: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_InstantMobSpawnAtPosDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantMobSpawnAtPosDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
