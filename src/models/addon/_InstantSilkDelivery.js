export default (db, types) => db.define('_InstantSilkDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    Silk: {
      type: types.INTEGER,
      allowNull: false
    },
    SilkGift: {
      type: types.INTEGER,
      allowNull: false
    },
    SilkPoint: {
      type: types.INTEGER,
      allowNull: false
    },
    SendPacket: {
      type: types.BOOLEAN,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_InstantSilkDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantSilkDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
