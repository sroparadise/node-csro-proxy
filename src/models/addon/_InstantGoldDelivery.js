export default (db, types) => db.define('_InstantGoldDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    Amount: {
      type: types.INTEGER,
      allowNull: true,
      defaultValue: 1000
    },
    ShowDeliveryMsg: {
      type: types.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
    sequelize: db,
    tableName: '_InstantGoldDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantGoldDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
