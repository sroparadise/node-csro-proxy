export default (db, types) => db.define('_InstantDmgDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: true
    },
    ReasonMask: {
      type: types.INTEGER,
      allowNull: true
    },
    Amount: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_InstantDmgDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantDmgDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
