export default (db, types) => db.define('_InstantPvpStateDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    State: {
      type: types.TINYINT,
      allowNull: false,
      defaultValue: 1
    }
  }, {
    sequelize: db,
    tableName: '_InstantPvpStateDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantPvpStateDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
