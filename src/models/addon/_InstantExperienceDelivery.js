export default (db, types) => db.define('_InstantExperienceDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    LevelExperience: {
      type: types.BIGINT,
      allowNull: false
    },
    SkillExperience: {
      type: types.BIGINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_InstantExperienceDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantExperienceDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
