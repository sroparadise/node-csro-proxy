export default (db, types) => db.define('_CustomNpcInteraction', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CodeName128: {
      type: types.STRING(128),
      allowNull: false
    },
    InteractionID: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_CustomNpcInteraction',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__CustomNpcInteraction",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
