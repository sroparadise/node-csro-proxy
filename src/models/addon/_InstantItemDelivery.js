export default (db, types) => db.define('_InstantItemDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    StorageType: {
      type: types.TINYINT,
      allowNull: true,
      defaultValue: 0
    },
    CodeName: {
      type: types.STRING(128),
      allowNull: false
    },
    Count: {
      type: types.INTEGER,
      allowNull: true,
      defaultValue: 1
    },
    Plus: {
      type: types.TINYINT,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize: db,
    tableName: '_InstantItemDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantItemDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
