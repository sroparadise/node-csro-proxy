export default (db, types) => db.define('_LogUniqueEntered', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Name: {
      type: types.STRING(128),
      allowNull: false
    },
    Time: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_LogUniqueEntered',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__LogUniqueEntered",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
