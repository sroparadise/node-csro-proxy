export default (db, types) => db.define('_InstantNoticeDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Notice: {
      type: types.TEXT,
      allowNull: false
    },
    SendPacket: {
      type: types.BOOLEAN,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_InstantNoticeDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantNoticeDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
