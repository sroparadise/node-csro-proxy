export default (db, types) => db.define('_LogUniqueKilled', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    UniqueName: {
      type: types.STRING(128),
      allowNull: false
    },
    KillerName: {
      type: types.STRING(32),
      allowNull: false
    },
    Time: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_LogUniqueKilled',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__LogUniqueKilled",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
