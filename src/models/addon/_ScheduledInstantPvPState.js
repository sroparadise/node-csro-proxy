export default (db, types) => db.define('_ScheduledInstantPvPState', {
    nID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szCharName: {
      type: types.TEXT,
      allowNull: false
    },
    nState: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_ScheduledInstantPvPState',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__ScheduledInstantPvPState",
        unique: true,
        fields: [
          { name: "nID" },
        ]
      },
    ]
});
