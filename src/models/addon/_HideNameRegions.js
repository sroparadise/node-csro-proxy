export default (db, types) => db.define('_HideNameRegions', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    RegionID: {
      type: types.SMALLINT,
      allowNull: false,
      defaultValue: 25000
    }
  }, {
    sequelize: db,
    tableName: '_HideNameRegions',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__HideNameRegions",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
