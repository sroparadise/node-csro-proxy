export default (db, types) => db.define('_InstantCharReloadDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_InstantCharReloadDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantCharReloadDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
