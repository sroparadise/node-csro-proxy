export default (db, types) => db.define('_ScheduledInstantItems', {
    nID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szCharname: {
      type: types.STRING(128),
      allowNull: false
    },
    szCodename: {
      type: types.STRING(128),
      allowNull: false
    },
    nItemCount: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_ScheduledInstantItems',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__ScheduledInstantItems",
        unique: true,
        fields: [
          { name: "nID" },
        ]
      },
    ]
});
