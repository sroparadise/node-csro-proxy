import _InstantNoticeDelivery from './_InstantNoticeDelivery';
import _InstantDmgDelivery from './_InstantDmgDelivery';

export {
    _InstantNoticeDelivery,
    _InstantDmgDelivery,
}