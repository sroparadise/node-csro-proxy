export default (db, types) => db.define('_InstantBuffsDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    CodeName128: {
      type: types.STRING(128),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_InstantBuffsDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantBuffsDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
