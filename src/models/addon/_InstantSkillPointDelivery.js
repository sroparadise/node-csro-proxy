export default (db, types) => db.define('_InstantSkillPointDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    Amount: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize: db,
    tableName: '_InstantSkillPointDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantSkillPointDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
