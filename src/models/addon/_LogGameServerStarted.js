export default (db, types) => db.define('_LogGameServerStarted', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ServerID: {
      type: types.INTEGER,
      allowNull: false
    },
    Time: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_LogGameServerStarted',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__LogGameServerStarted",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
