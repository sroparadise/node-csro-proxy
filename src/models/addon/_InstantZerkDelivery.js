export default (db, types) => db.define('_InstantZerkDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    Points: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_InstantZerkDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantZerkDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
