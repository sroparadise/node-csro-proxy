export default (db, types) => db.define('_InstantMobSpawnNearPlayerDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    Type: {
      type: types.TINYINT,
      allowNull: false
    },
    Range: {
      type: types.FLOAT,
      allowNull: false
    },
    RefObjID: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_InstantMobSpawnNearPlayerDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantMobSpawnNearPlayerDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
