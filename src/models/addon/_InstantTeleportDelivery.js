export default (db, types) => db.define('_InstantTeleportDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    WorldLayerID: {
      type: types.SMALLINT,
      allowNull: false,
      defaultValue: 1
    },
    WorldID: {
      type: types.SMALLINT,
      allowNull: false,
      defaultValue: 1
    },
    RegionID: {
      type: types.SMALLINT,
      allowNull: false
    },
    PosX: {
      type: types.FLOAT,
      allowNull: false
    },
    PosY: {
      type: types.FLOAT,
      allowNull: false
    },
    PosZ: {
      type: types.FLOAT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_InstantTeleportDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantTeleportDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
