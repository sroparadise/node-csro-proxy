export default (db, types) => db.define('_InstantStatPointDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    FreeStatPoints: {
      type: types.SMALLINT,
      allowNull: false
    },
    Str: {
      type: types.SMALLINT,
      allowNull: false
    },
    Int: {
      type: types.SMALLINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_InstantStatPointDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantFreeStatPointDelivery",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
