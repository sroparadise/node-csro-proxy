export default (db, types) => db.define('_InstantHwanLevelDelivery', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    Level: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_InstantHwanLevelDelivery',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__InstantHwanLevel",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
