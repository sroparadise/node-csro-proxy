export default (db, types) => db.define('_AlliedClans', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Ally1: {
      type: types.INTEGER,
      allowNull: true
    },
    Ally2: {
      type: types.INTEGER,
      allowNull: true
    },
    Ally3: {
      type: types.INTEGER,
      allowNull: true
    },
    Ally4: {
      type: types.INTEGER,
      allowNull: true
    },
    Ally5: {
      type: types.INTEGER,
      allowNull: true
    },
    Ally6: {
      type: types.INTEGER,
      allowNull: true
    },
    Ally7: {
      type: types.INTEGER,
      allowNull: true
    },
    Ally8: {
      type: types.INTEGER,
      allowNull: true
    },
    FoundationDate: {
      type: "SMALLDATETIME",
      allowNull: false
    },
    LastCrestRev: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    CurCrestRev: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize: db,
    tableName: '_AlliedClans',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__AlliedClans",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
