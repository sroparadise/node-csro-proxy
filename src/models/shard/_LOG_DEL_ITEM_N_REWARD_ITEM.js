export default (db, types) => db.define('_LOG_DEL_ITEM_N_REWARD_ITEM', {
    RewardDate: {
      type: "SMALLDATETIME",
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('getdate')
    },
    StorageType: {
      type: types.STRING(64),
      allowNull: false
    },
    OwnerID: {
      type: types.INTEGER,
      allowNull: false
    },
    DelItemCodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    DelID64: {
      type: types.BIGINT,
      allowNull: false
    },
    DelItemCnt: {
      type: types.INTEGER,
      allowNull: false
    },
    RewardCodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    RewardID64: {
      type: types.BIGINT,
      allowNull: false
    },
    RewardItemCnt: {
      type: types.INTEGER,
      allowNull: false
    },
    IsReward: {
      type: types.STRING(2),
      allowNull: false
    },
    ErrorCode: {
      type: types.INTEGER,
      allowNull: false
    },
    strDesc: {
      type: types.STRING(129),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_LOG_DEL_ITEM_N_REWARD_ITEM',
    schema: 'dbo',
    timestamps: false
});
