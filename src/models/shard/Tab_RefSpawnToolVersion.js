export default (db, types) => db.define('Tab_RefSpawnToolVersion', {
    dwRefDataVersion: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szVersionDescString: {
      type: types.STRING(128),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'Tab_RefSpawnToolVersion',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__Tab_RefSpawnTool__6A30C649",
        unique: true,
        fields: [
          { name: "dwRefDataVersion" },
        ]
      },
    ]
});
