export default (db, types) => db.define('_RefRewardPackageItems', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    QuestID: {
      type: types.INTEGER,
      allowNull: false
    },
    PackageRefItemID: {
      type: types.INTEGER,
      allowNull: false
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefRewardPackageItems',
    schema: 'dbo',
    timestamps: false
});
