export default (db, types) => db.define('__Statistics_SystemExchageCoupon', {
    Target: {
      type: types.INTEGER,
      allowNull: true
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: true
    },
    Count: {
      type: types.BIGINT,
      allowNull: true
    },
    EventTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__Statistics_SystemExchageCoupon',
    schema: 'dbo',
    timestamps: false
});
