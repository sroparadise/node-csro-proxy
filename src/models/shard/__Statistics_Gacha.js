export default (db, types) => db.define('__Statistics_Gacha', {
    nIdx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szCodeName128: {
      type: types.STRING(128),
      allowNull: true
    },
    szObjName128: {
      type: types.STRING(128),
      allowNull: true
    },
    nCount: {
      type: types.INTEGER,
      allowNull: true
    },
    dEventTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__Statistics_Gacha',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK____Statistics_Gac__2DE6D218",
        unique: true,
        fields: [
          { name: "nIdx" },
        ]
      },
    ]
});
