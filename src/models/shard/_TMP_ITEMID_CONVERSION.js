export default (db, types) => db.define('_TMP_ITEMID_CONVERSION', {
    ShardID: {
      type: types.SMALLINT,
      allowNull: false,
      primaryKey: true
    },
    OldID: {
      type: types.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    NewID: {
      type: types.BIGINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_TMP_ITEMID_CONVERSION',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__TMP_ITEMID_CONVERSION",
        unique: true,
        fields: [
          { name: "ShardID" },
          { name: "OldID" },
        ]
      },
    ]
});
