export default (db, types) => db.define('_RefAlchemy_MK_Shop', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    Shop_CodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    RC_ID: {
      type: types.INTEGER,
      allowNull: false,
      references: {
        model: '_RefAlchemy_MK_Recipe',
        key: 'RC_ID'
      }
    },
    Cost: {
      type: types.BIGINT,
      allowNull: false
    },
    Param1: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    Param1_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param2: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    Param2_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param3: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    Param3_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param4: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    Param4_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param5: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    Param5_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    }
  }, {
    sequelize: db,
    tableName: '_RefAlchemy_MK_Shop',
    schema: 'dbo',
    timestamps: false
});
