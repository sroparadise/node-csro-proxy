export default (db, types) => db.define('__Statistics_RentItem', {
    nIdx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nRentType: {
      type: types.TINYINT,
      allowNull: true
    },
    szCodename128: {
      type: types.STRING(128),
      allowNull: true
    },
    nCount: {
      type: types.INTEGER,
      allowNull: true
    },
    dEventTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__Statistics_RentItem',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK____Statistics_Ren__467D75B8",
        unique: true,
        fields: [
          { name: "nIdx" },
        ]
      },
    ]
});
