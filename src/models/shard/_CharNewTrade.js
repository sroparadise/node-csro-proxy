export default (db, types) => db.define('_CharNewTrade', {
    CharID: {
      type: types.INTEGER,
      allowNull: true,
      unique: "UQ___CharNewTrade__40058253"
    },
    TradeState: {
      type: types.TINYINT,
      allowNull: false
    },
    DepartureRegionID: {
      type: types.SMALLINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_CharNewTrade',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "UQ___CharNewTrade__40058253",
        unique: true,
        fields: [
          { name: "CharID" },
        ]
      },
    ]
});
