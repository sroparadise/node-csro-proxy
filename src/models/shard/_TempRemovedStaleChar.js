export default (db, types) => db.define('_TempRemovedStaleChar', {
    CharID: {
      type: types.INTEGER,
      allowNull: true
    },
    UserJID: {
      type: types.INTEGER,
      allowNull: true
    },
    CharName: {
      type: types.STRING(17),
      allowNull: true
    },
    DeletedDate: {
      type: types.DATE,
      allowNull: true
    },
    strDesc: {
      type: types.STRING(256),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_TempRemovedStaleChar',
    schema: 'dbo',
    timestamps: false
});
