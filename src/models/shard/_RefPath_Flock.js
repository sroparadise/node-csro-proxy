export default (db, types) => db.define('_RefPath_Flock', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    PathID: {
      type: types.INTEGER,
      allowNull: false,
      references: {
        model: '_RefPath',
        key: 'ID'
      }
    },
    PositionRememberType: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefPath_Flock',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefPath_Flock__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
