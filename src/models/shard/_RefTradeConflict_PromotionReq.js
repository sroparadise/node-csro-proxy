export default (db, types) => db.define('_RefTradeConflict_PromotionReq', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    PromotionPhase: {
      type: types.TINYINT,
      allowNull: false
    },
    ReqType: {
      type: types.TINYINT,
      allowNull: false
    },
    Param1_128: {
      type: types.STRING(129),
      allowNull: false
    },
    Param2_128: {
      type: types.STRING(129),
      allowNull: false
    },
    Param3_128: {
      type: types.STRING(129),
      allowNull: false
    },
    Param4_128: {
      type: types.STRING(129),
      allowNull: false
    },
    Param5_128: {
      type: types.STRING(129),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefTradeConflict_PromotionReq',
    schema: 'dbo',
    timestamps: false
});
