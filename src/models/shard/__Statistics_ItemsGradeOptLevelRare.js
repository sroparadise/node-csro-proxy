export default (db, types) => db.define('__Statistics_ItemsGradeOptLevelRare', {
    nIdx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nGrade: {
      type: types.TINYINT,
      allowNull: true
    },
    nOptLevel: {
      type: types.TINYINT,
      allowNull: true
    },
    nCount: {
      type: types.INTEGER,
      allowNull: true
    },
    dEventTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__Statistics_ItemsGradeOptLevelRare',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX___Statistics_ItemsGradeOptLevelRare_dEventTime",
        fields: [
          { name: "dEventTime" },
        ]
      },
      {
        name: "PK____Statistics_Ite__4E1E9780",
        unique: true,
        fields: [
          { name: "nIdx" },
        ]
      },
    ]
});
