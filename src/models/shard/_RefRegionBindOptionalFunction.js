export default (db, types) => db.define('_RefRegionBindOptionalFunction', {
    WorldID: {
      type: types.INTEGER,
      allowNull: false
    },
    RegionID: {
      type: types.INTEGER,
      allowNull: false
    },
    FunctionID: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefRegionBindOptionalFunction',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__RefRegionBindOptionalFunction",
        fields: [
          { name: "RegionID" },
          { name: "WorldID" },
        ]
      },
    ]
});
