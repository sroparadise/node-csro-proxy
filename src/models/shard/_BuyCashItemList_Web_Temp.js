export default (db, types) => db.define('_BuyCashItemList_Web_Temp', {
    IDX: {
      type: types.BIGINT,
      allowNull: false
    },
    JID: {
      type: types.INTEGER,
      allowNull: false
    },
    Section: {
      type: types.INTEGER,
      allowNull: false
    },
    PackageCodeName: {
      type: types.STRING(128),
      allowNull: true
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: false
    },
    ItemCount: {
      type: types.INTEGER,
      allowNull: false
    },
    RegDate: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_BuyCashItemList_Web_Temp',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__BuyCashItemList_By_Web_JID_Temp",
        fields: [
          { name: "JID" },
        ]
      },
    ]
});
