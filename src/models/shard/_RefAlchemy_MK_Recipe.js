export default (db, types) => db.define('_RefAlchemy_MK_Recipe', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    RC_ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    RC_CodeName128: {
      type: types.STRING(129),
      allowNull: false
    },
    RC_Desc128: {
      type: types.STRING(129),
      allowNull: false
    },
    RC_StrID128: {
      type: types.STRING(129),
      allowNull: false
    },
    Result_StrID128: {
      type: types.STRING(129),
      allowNull: false
    },
    RC_Icon: {
      type: types.STRING(129),
      allowNull: false
    },
    RC_Tab: {
      type: types.TINYINT,
      allowNull: false
    },
    RC_Slot: {
      type: types.TINYINT,
      allowNull: false
    },
    Require_CharLevel: {
      type: types.TINYINT,
      allowNull: false
    },
    RC_Level: {
      type: types.TINYINT,
      allowNull: false
    },
    RC_Rarity: {
      type: types.TINYINT,
      allowNull: false
    },
    RC_Type: {
      type: types.TINYINT,
      allowNull: false
    },
    RC_Reuse_Time: {
      type: types.INTEGER,
      allowNull: false
    },
    RC_Make_Time: {
      type: types.INTEGER,
      allowNull: false
    },
    MK_MaterialGroup1: {
      type: types.STRING(129),
      allowNull: false
    },
    MtGroup1_Condition: {
      type: types.INTEGER,
      allowNull: false
    },
    MK_MaterialGroup2: {
      type: types.STRING(129),
      allowNull: false
    },
    MtGroup2_Condition: {
      type: types.INTEGER,
      allowNull: false
    },
    MK_MaterialGroup3: {
      type: types.STRING(129),
      allowNull: false
    },
    MtGroup3_Condition: {
      type: types.INTEGER,
      allowNull: false
    },
    MK_ResultGroup1: {
      type: types.STRING(129),
      allowNull: false
    },
    RtGroup1_Condition: {
      type: types.INTEGER,
      allowNull: false
    },
    MK_ResultGroup2: {
      type: types.STRING(129),
      allowNull: false
    },
    RtGroup2_Condition: {
      type: types.INTEGER,
      allowNull: false
    },
    MK_ResultGroup3: {
      type: types.STRING(129),
      allowNull: false
    },
    RtGroup3_Condition: {
      type: types.INTEGER,
      allowNull: false
    },
    Param1: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    Param1_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param2: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    Param2_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param3: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    Param3_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    }
  }, {
    sequelize: db,
    tableName: '_RefAlchemy_MK_Recipe',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_RefAlchemy_MK_Recipe",
        unique: true,
        fields: [
          { name: "RC_ID" },
        ]
      },
    ]
});
