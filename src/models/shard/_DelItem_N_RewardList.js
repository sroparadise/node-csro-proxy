export default (db, types) => db.define('_DelItem_N_RewardList', {
    CodeName128: {
      type: types.STRING(128),
      allowNull: false
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: false
    },
    RewardType: {
      type: types.TINYINT,
      allowNull: false
    },
    RewardAmount: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_DelItem_N_RewardList',
    schema: 'dbo',
    timestamps: false
});
