export default (db, types) => db.define('_TradeBagInventory', {
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_Char',
        key: 'CharID'
      }
    },
    Slot: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    ItemID: {
      type: types.BIGINT,
      allowNull: false,
      references: {
        model: '_Items',
        key: 'ID64'
      }
    }
  }, {
    sequelize: db,
    tableName: '_TradeBagInventory',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__TradeBagInventory",
        unique: true,
        fields: [
          { name: "CharID" },
          { name: "Slot" },
        ]
      },
    ]
});
