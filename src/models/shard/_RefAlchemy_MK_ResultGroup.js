export default (db, types) => db.define('_RefAlchemy_MK_ResultGroup', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    GroupCodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    ItemCodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    Make_Count: {
      type: types.TINYINT,
      allowNull: false
    },
    Make_Ratio: {
      type: types.REAL,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefAlchemy_MK_ResultGroup',
    schema: 'dbo',
    timestamps: false
});
