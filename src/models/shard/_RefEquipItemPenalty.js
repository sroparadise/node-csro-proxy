export default (db, types) => db.define('_RefEquipItemPenalty', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    PenaltyEquipType: {
      type: types.TINYINT,
      allowNull: false
    },
    PenaltyLevel: {
      type: types.TINYINT,
      allowNull: false
    },
    ItemClass: {
      type: types.TINYINT,
      allowNull: false
    },
    PenaltyClass: {
      type: types.TINYINT,
      allowNull: false
    },
    MultipleValue: {
      type: types.SMALLINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefEquipItemPenalty',
    schema: 'dbo',
    timestamps: false
});
