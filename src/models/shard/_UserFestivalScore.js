export default (db, types) => db.define('_UserFestivalScore', {
    UserJID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_AccountJID',
        key: 'JID'
      }
    },
    AccountID: {
      type: types.STRING(128),
      allowNull: false
    },
    FestivalScore: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_UserFestivalScore',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__UserFestivalScore",
        unique: true,
        fields: [
          { name: "UserJID" },
        ]
      },
    ]
});
