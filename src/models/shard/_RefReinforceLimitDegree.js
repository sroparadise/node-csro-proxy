export default (db, types) => db.define('_RefReinforceLimitDegree', {
    RefItemID: {
      type: types.INTEGER,
      allowNull: false
    },
    Degree: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefReinforceLimitDegree',
    schema: 'dbo',
    timestamps: false
});
