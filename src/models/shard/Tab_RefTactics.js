export default (db, types) => db.define('Tab_RefTactics', {
    dwTacticsID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    dwObjID: {
      type: types.INTEGER,
      allowNull: false
    },
    btAIQoS: {
      type: types.TINYINT,
      allowNull: false
    },
    nMaxStamina: {
      type: types.INTEGER,
      allowNull: false
    },
    btMaxStaminaVariance: {
      type: types.TINYINT,
      allowNull: false
    },
    nSightRange: {
      type: types.INTEGER,
      allowNull: false
    },
    btAggressType: {
      type: types.TINYINT,
      allowNull: false
    },
    AggressData: {
      type: types.INTEGER,
      allowNull: false
    },
    btChangeTarget: {
      type: types.TINYINT,
      allowNull: false
    },
    btHelpRequestTo: {
      type: types.TINYINT,
      allowNull: false
    },
    btHelpResponseTo: {
      type: types.TINYINT,
      allowNull: false
    },
    btBattleStyle: {
      type: types.TINYINT,
      allowNull: false
    },
    BattleStyleData: {
      type: types.INTEGER,
      allowNull: false
    },
    btDiversionBasis: {
      type: types.TINYINT,
      allowNull: false
    },
    DiversionBasisData1: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionBasisData2: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionBasisData3: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionBasisData4: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionBasisData5: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionBasisData6: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionBasisData7: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionBasisData8: {
      type: types.INTEGER,
      allowNull: false
    },
    btDiversionKeepBasis: {
      type: types.TINYINT,
      allowNull: false
    },
    DiversionKeepBasisData1: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionKeepBasisData2: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionKeepBasisData3: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionKeepBasisData4: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionKeepBasisData5: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionKeepBasisData6: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionKeepBasisData7: {
      type: types.INTEGER,
      allowNull: false
    },
    DiversionKeepBasisData8: {
      type: types.INTEGER,
      allowNull: false
    },
    btKeepDistance: {
      type: types.TINYINT,
      allowNull: false
    },
    KeepDistanceData: {
      type: types.INTEGER,
      allowNull: false
    },
    btTraceType: {
      type: types.TINYINT,
      allowNull: false
    },
    btTraceBoundary: {
      type: types.TINYINT,
      allowNull: false
    },
    TraceData: {
      type: types.INTEGER,
      allowNull: false
    },
    btHomingType: {
      type: types.TINYINT,
      allowNull: false
    },
    HomingData: {
      type: types.INTEGER,
      allowNull: false
    },
    btAggressTypeOnHoming: {
      type: types.TINYINT,
      allowNull: false
    },
    btFleeType: {
      type: types.TINYINT,
      allowNull: false
    },
    dwChampionTacticsID: {
      type: types.INTEGER,
      allowNull: false
    },
    AdditionOptionFlag: {
      type: types.INTEGER,
      allowNull: false
    },
    szDescString128: {
      type: types.STRING(129),
      allowNull: false
    },
    btAggroType: {
      type: types.TINYINT,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize: db,
    tableName: 'Tab_RefTactics',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__Tab_RefTactics__62065FF3",
        unique: true,
        fields: [
          { name: "dwTacticsID" },
        ]
      },
    ]
});
