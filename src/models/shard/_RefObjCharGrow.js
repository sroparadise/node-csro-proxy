export default (db, types) => db.define('_RefObjCharGrow', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    StrDef: {
      type: types.SMALLINT,
      allowNull: false
    },
    InitDef: {
      type: types.SMALLINT,
      allowNull: false
    },
    StrGrow: {
      type: types.SMALLINT,
      allowNull: false
    },
    IntGrow: {
      type: types.SMALLINT,
      allowNull: false
    },
    Lv1HP: {
      type: types.SMALLINT,
      allowNull: false
    },
    LvUpHP: {
      type: types.SMALLINT,
      allowNull: false
    },
    SatietyMax: {
      type: types.INTEGER,
      allowNull: false
    },
    SatietyConsume: {
      type: types.SMALLINT,
      allowNull: false
    },
    ScaleRate: {
      type: types.SMALLINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefObjCharGrow',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefObjCharGrow",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
