export default (db, types) => db.define('_KeepDelCharName', {
    CharName: {
      type: types.STRING(64),
      allowNull: false,
      primaryKey: true
    },
    DelTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_KeepDelCharName',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__KeepDelCHarName_CharName",
        unique: true,
        fields: [
          { name: "CharName" },
        ]
      },
    ]
});
