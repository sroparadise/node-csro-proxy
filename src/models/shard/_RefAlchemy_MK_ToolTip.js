export default (db, types) => db.define('_RefAlchemy_MK_ToolTip', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    RC_CodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    Description: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: ""
    },
    Material_StrName1: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Material_StrName1_Count: {
      type: types.INTEGER,
      allowNull: false
    },
    Material_StrName2: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Material_StrName2_Count: {
      type: types.INTEGER,
      allowNull: false
    },
    Material_StrName3: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Material_StrName3_Count: {
      type: types.INTEGER,
      allowNull: false
    },
    Material_StrName4: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Material_StrName4_Count: {
      type: types.INTEGER,
      allowNull: false
    },
    Material_StrName5: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Material_StrName5_Count: {
      type: types.INTEGER,
      allowNull: false
    },
    Material_StrName6: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Material_StrName6_Count: {
      type: types.INTEGER,
      allowNull: false
    },
    ResultType: {
      type: types.TINYINT,
      allowNull: false
    },
    ResultItem_StrName: {
      type: types.STRING(129),
      allowNull: false
    },
    ResultItem_Count: {
      type: types.INTEGER,
      allowNull: false
    },
    ResultItem_ToolTip: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: ""
    },
    ResultItem_Icon: {
      type: types.STRING(129),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefAlchemy_MK_ToolTip',
    schema: 'dbo',
    timestamps: false
});
