export default (db, types) => db.define('_RefEventFestivalReward', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    RewardLevel: {
      type: types.TINYINT,
      allowNull: false
    },
    ItemCodeName: {
      type: types.STRING(128),
      allowNull: false,
      primaryKey: true
    },
    Quantity: {
      type: types.TINYINT,
      allowNull: false
    },
    Ratio: {
      type: types.REAL,
      allowNull: false
    },
    Param1: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Param1_Desc: {
      type: types.STRING(128),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param2: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Param2_Desc: {
      type: types.STRING(128),
      allowNull: false,
      defaultValue: "xxx"
    }
  }, {
    sequelize: db,
    tableName: '_RefEventFestivalReward',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefEventFestivalReward",
        unique: true,
        fields: [
          { name: "ItemCodeName" },
        ]
      },
    ]
});
