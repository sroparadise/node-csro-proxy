export default (db, types) => db.define('_TempChest', {
    UserJID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_AccountJID',
        key: 'JID'
      }
    },
    ShardID: {
      type: types.SMALLINT,
      allowNull: false,
      primaryKey: true
    },
    Slot: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    ItemID: {
      type: types.BIGINT,
      allowNull: false,
      references: {
        model: '_Items',
        key: 'ID64'
      }
    }
  }, {
    sequelize: db,
    tableName: '_TempChest',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__TEMPCHEST",
        unique: true,
        fields: [
          { name: "UserJID" },
          { name: "ShardID" },
          { name: "Slot" },
        ]
      },
    ]
});
