export default (db, types) => db.define('_RefExtraAbilityByEquipItemOptLevel', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    OptLevel: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    Phy_Inc_Opt: {
      type: types.REAL,
      allowNull: false
    },
    Mgc_Inc_Opt: {
      type: types.REAL,
      allowNull: false
    },
    MgcOptID1: {
      type: types.SMALLINT,
      allowNull: false
    },
    MgcOptLevel1: {
      type: types.INTEGER,
      allowNull: false
    },
    MgcOptValue1: {
      type: types.INTEGER,
      allowNull: false
    },
    MgcOptID2: {
      type: types.SMALLINT,
      allowNull: false
    },
    MgcOptLevel2: {
      type: types.INTEGER,
      allowNull: false
    },
    MgcOptValue2: {
      type: types.INTEGER,
      allowNull: false
    },
    MgcOptID3: {
      type: types.SMALLINT,
      allowNull: false
    },
    MgcOptLevel3: {
      type: types.INTEGER,
      allowNull: false
    },
    MgcOptValue3: {
      type: types.INTEGER,
      allowNull: false
    },
    MgcOptID4: {
      type: types.SMALLINT,
      allowNull: false
    },
    MgcOptLevel4: {
      type: types.INTEGER,
      allowNull: false
    },
    MgcOptValue4: {
      type: types.INTEGER,
      allowNull: false
    },
    MgcOptID5: {
      type: types.SMALLINT,
      allowNull: false
    },
    MgcOptLevel5: {
      type: types.INTEGER,
      allowNull: false
    },
    MgcOptValue5: {
      type: types.INTEGER,
      allowNull: false
    },
    SkillID1: {
      type: types.INTEGER,
      allowNull: false
    },
    SkillID2: {
      type: types.INTEGER,
      allowNull: false
    },
    SkillID3: {
      type: types.INTEGER,
      allowNull: false
    },
    SkillID4: {
      type: types.INTEGER,
      allowNull: false
    },
    SkillID5: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefExtraAbilityByEquipItemOptLevel',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefExtraAbilityByEquipItemOptLevel",
        unique: true,
        fields: [
          { name: "RefItemID" },
          { name: "OptLevel" },
        ]
      },
    ]
});
