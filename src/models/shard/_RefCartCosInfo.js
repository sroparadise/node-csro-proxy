export default (db, types) => db.define('_RefCartCosInfo', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    id: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CartCosCodeName: {
      type: types.STRING(128),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefCartCosInfo',
    schema: 'dbo',
    timestamps: false
});
