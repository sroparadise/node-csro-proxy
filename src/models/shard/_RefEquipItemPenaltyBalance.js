export default (db, types) => db.define('_RefEquipItemPenaltyBalance', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    TypeID2: {
      type: types.TINYINT,
      allowNull: false
    },
    TypeID3: {
      type: types.TINYINT,
      allowNull: false
    },
    TypeID4: {
      type: types.TINYINT,
      allowNull: false
    },
    PenaltyClass: {
      type: types.TINYINT,
      allowNull: false
    },
    Penalty_Phy_Min: {
      type: types.REAL,
      allowNull: false
    },
    Penalty_Phy_Max: {
      type: types.REAL,
      allowNull: false
    },
    Penalty_Mgc_Min: {
      type: types.REAL,
      allowNull: false
    },
    Penalty_Mgc_Max: {
      type: types.REAL,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefEquipItemPenaltyBalance',
    schema: 'dbo',
    timestamps: false
});
