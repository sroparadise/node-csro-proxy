export default (db, types) => db.define('_GuildChest', {
    GuildID: {
      type: types.INTEGER,
      allowNull: false
    },
    Slot: {
      type: types.TINYINT,
      allowNull: false
    },
    ItemID: {
      type: types.BIGINT,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_GuildChest',
    schema: 'dbo',
    timestamps: false
});
