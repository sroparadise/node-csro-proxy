export default (db, types) => db.define('_RefShopObject', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CodeName128: {
      type: types.STRING(129),
      allowNull: false,
      unique: "IX__RefShopObject_1"
    }
  }, {
    sequelize: db,
    tableName: '_RefShopObject',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__RefShopObject",
        unique: true,
        fields: [
          { name: "CodeName128" },
        ]
      },
      {
        name: "IX__RefShopObject_1",
        unique: true,
        fields: [
          { name: "CodeName128" },
        ]
      },
      {
        name: "PK__RefShopObject",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
