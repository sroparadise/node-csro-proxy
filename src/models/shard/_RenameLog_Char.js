export default (db, types) => db.define('_RenameLog_Char', {
    OldName: {
      type: types.STRING(64),
      allowNull: false,
      primaryKey: true
    },
    NewName: {
      type: types.STRING(64),
      allowNull: false
    },
    RenameTime: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RenameLog_Char',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__RenameLog_Char",
        fields: [
          { name: "NewName" },
        ]
      },
      {
        name: "PK__RenameLog_Char",
        unique: true,
        fields: [
          { name: "OldName" },
        ]
      },
    ]
});
