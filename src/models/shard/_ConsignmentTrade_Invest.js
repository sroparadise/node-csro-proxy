export default (db, types) => db.define('_ConsignmentTrade_Invest', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    ProgressID: {
      type: types.INTEGER,
      allowNull: false
    },
    RefObjID: {
      type: types.INTEGER,
      allowNull: false
    },
    Count: {
      type: types.INTEGER,
      allowNull: false
    },
    EventTime: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_ConsignmentTrade_Invest',
    schema: 'dbo',
    timestamps: false
});
