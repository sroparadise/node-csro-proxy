export default (db, types) => db.define('WebMall_OrderInfo', {
    InfoID: {
      type: types.INTEGER,
      allowNull: false
    },
    OrderID: {
      type: types.INTEGER,
      allowNull: false
    },
    ProID: {
      type: types.INTEGER,
      allowNull: false
    },
    TradeNum: {
      type: types.INTEGER,
      allowNull: false
    },
    StrUserID: {
      type: types.STRING(50),
      allowNull: false
    },
    ShardID: {
      type: types.INTEGER,
      allowNull: false
    },
    Web_State: {
      type: types.INTEGER,
      allowNull: false
    },
    DB_State: {
      type: types.INTEGER,
      allowNull: false
    },
    InTime: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'WebMall_OrderInfo',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IDX__InfoID_OrderInfo",
        fields: [
          { name: "InfoID", order: "DESC" },
        ]
      },
      {
        name: "idx_StrUserID_WebMall_OrderInfo",
        fields: [
          { name: "StrUserID" },
        ]
      },
    ]
});
