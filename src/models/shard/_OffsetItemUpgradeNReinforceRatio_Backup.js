export default (db, types) => db.define('_OffsetItemUpgradeNReinforceRatio_Backup', {
    RatioType: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    RatioSubType: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    Ratio: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_OffsetItemUpgradeNReinforceRatio_Backup',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__OffsetItemUpgradeNReinforceRatio_Backup",
        unique: true,
        fields: [
          { name: "RatioType" },
          { name: "RatioSubType" },
        ]
      },
    ]
});
