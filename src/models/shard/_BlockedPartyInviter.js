export default (db, types) => db.define('_BlockedPartyInviter', {
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    BlockedName: {
      type: types.STRING(64),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_BlockedPartyInviter',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "BP_NIDX_CID_BN",
        fields: [
          { name: "CharID" },
          { name: "BlockedName" },
        ]
      },
    ]
});
