export default (db, types) => db.define('_RefPath_Position_Config_Group', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    GroupCodename: {
      type: types.STRING(128),
      allowNull: false,
      unique: "UK__RefPath_Position_Config_Group__GroupCodename"
    }
  }, {
    sequelize: db,
    tableName: '_RefPath_Position_Config_Group',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefPath_Position_Config_Group__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "UK__RefPath_Position_Config_Group__GroupCodename",
        unique: true,
        fields: [
          { name: "GroupCodename" },
        ]
      },
    ]
});
