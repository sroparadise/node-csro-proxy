export default (db, types) => db.define('_ConsignmentTrade_Progress', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    JobType: {
      type: types.TINYINT,
      allowNull: false
    },
    State: {
      type: types.TINYINT,
      allowNull: false
    },
    PathID: {
      type: types.INTEGER,
      allowNull: false
    },
    FlockID: {
      type: types.INTEGER,
      allowNull: false
    },
    Tiredness: {
      type: types.INTEGER,
      allowNull: false
    },
    TotalCount: {
      type: types.INTEGER,
      allowNull: false
    },
    RemainCount: {
      type: types.INTEGER,
      allowNull: false
    },
    Magnification: {
      type: types.TINYINT,
      allowNull: false
    },
    EventTime: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_ConsignmentTrade_Progress',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__ConsignmentTrade_Progress__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
