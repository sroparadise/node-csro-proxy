export default (db, types) => db.define('_RefPath_Position', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    PathID: {
      type: types.INTEGER,
      allowNull: false,
      references: {
        model: '_RefPath',
        key: 'ID'
      }
    },
    Sequence: {
      type: types.INTEGER,
      allowNull: false
    },
    Codename: {
      type: types.STRING(128),
      allowNull: false
    },
    RegionID: {
      type: types.SMALLINT,
      allowNull: false
    },
    PosX: {
      type: types.REAL,
      allowNull: false
    },
    PosY: {
      type: types.REAL,
      allowNull: false
    },
    PosZ: {
      type: types.REAL,
      allowNull: false
    },
    ConfigGroupID: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0,
      references: {
        model: '_RefPath_Position_Config_Group',
        key: 'ID'
      }
    }
  }, {
    sequelize: db,
    tableName: '_RefPath_Position',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefPath_Position__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
