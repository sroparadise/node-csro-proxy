export default (db, types) => db.define('_RefDropClassSel_COSSkillRemove', {
    MonLevel: {
      type: types.INTEGER,
      allowNull: false
    },
    ProbGroup1: {
      type: types.REAL,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefDropClassSel_COSSkillRemove',
    schema: 'dbo',
    timestamps: false
});
