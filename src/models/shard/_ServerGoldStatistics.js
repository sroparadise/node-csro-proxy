export default (db, types) => db.define('_ServerGoldStatistics', {
    SG_IDX: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    SG_EventTime: {
      type: types.DATE,
      allowNull: false
    },
    SG_CharCount: {
      type: types.INTEGER,
      allowNull: false
    },
    SG_AccountCount: {
      type: types.INTEGER,
      allowNull: false
    },
    SG_GuildCount: {
      type: types.INTEGER,
      allowNull: false
    },
    SG_HunterCount: {
      type: types.INTEGER,
      allowNull: false
    },
    SG_RobberCount: {
      type: types.INTEGER,
      allowNull: false
    },
    SG_Char: {
      type: types.BIGINT,
      allowNull: false
    },
    SG_Chest: {
      type: types.BIGINT,
      allowNull: false
    },
    SG_GuildChest: {
      type: types.BIGINT,
      allowNull: false
    },
    SG_GuildWarBooty: {
      type: types.BIGINT,
      allowNull: false
    },
    SG_GuildWarLodgedGold: {
      type: types.BIGINT,
      allowNull: false
    },
    SG_HunterReward: {
      type: types.BIGINT,
      allowNull: false
    },
    SG_RobberReward: {
      type: types.BIGINT,
      allowNull: false
    },
    SG_FortressJanganTax: {
      type: types.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    SG_FortressDonwhangTax: {
      type: types.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    SG_FortressHotanTax: {
      type: types.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    SG_FortressConstantiopleTax: {
      type: types.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    SG_FortressSamarkandTax: {
      type: types.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    SG_FortressBijeokdanTax: {
      type: types.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    SG_FortressHeukmakdanTax: {
      type: types.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    SG_FortressEvilorderTax: {
      type: types.BIGINT,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize: db,
    tableName: '_ServerGoldStatistics',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__ServerGoldStatistics_SG_IDX",
        unique: true,
        fields: [
          { name: "SG_IDX" },
        ]
      },
    ]
});
