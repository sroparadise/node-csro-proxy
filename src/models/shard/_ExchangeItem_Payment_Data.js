export default (db, types) => db.define('_ExchangeItem_Payment_Data', {
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Slot: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: false
    },
    Data: {
      type: types.INTEGER,
      allowNull: false
    },
    OptLevel: {
      type: types.TINYINT,
      allowNull: false
    },
    Variance: {
      type: types.BIGINT,
      allowNull: false
    },
    MagicParamNum: {
      type: types.TINYINT,
      allowNull: false
    },
    MagParam1: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam2: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam3: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam4: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam5: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam6: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam7: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam8: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam9: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam10: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam11: {
      type: types.BIGINT,
      allowNull: false
    },
    MagParam12: {
      type: types.BIGINT,
      allowNull: false
    },
    RentCodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    MeterRateTime: {
      type: "SMALLDATETIME",
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_ExchangeItem_Payment_Data',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__ExchangeItem_Payment_Data",
        unique: true,
        fields: [
          { name: "CharID" },
          { name: "Slot" },
        ]
      },
    ]
});
