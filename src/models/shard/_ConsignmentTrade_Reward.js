export default (db, types) => db.define('_ConsignmentTrade_Reward', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    ProgressID: {
      type: types.INTEGER,
      allowNull: false
    },
    InvestCount: {
      type: types.INTEGER,
      allowNull: false
    },
    TradeTotalCount: {
      type: types.INTEGER,
      allowNull: false
    },
    RemainCount: {
      type: types.INTEGER,
      allowNull: false
    },
    Ratio: {
      type: types.STRING(128),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_ConsignmentTrade_Reward',
    schema: 'dbo',
    timestamps: false
});
