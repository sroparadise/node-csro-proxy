export default (db, types) => db.define('_RefPath_Config_Type', {
    ConfigCodename: {
      type: types.STRING(128),
      allowNull: false,
      primaryKey: true
    },
    Type: {
      type: types.STRING(32),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefPath_Config_Type',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefPath_Config_Type__Codename",
        unique: true,
        fields: [
          { name: "ConfigCodename" },
        ]
      },
    ]
});
