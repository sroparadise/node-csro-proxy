export default (db, types) => db.define('_RefPath_FlockMember', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    FlockID: {
      type: types.INTEGER,
      allowNull: false,
      references: {
        model: '_RefPath_Flock',
        key: 'ID'
      }
    },
    TacticsID: {
      type: types.INTEGER,
      allowNull: false
    },
    MemberType: {
      type: types.TINYINT,
      allowNull: false
    },
    OffsetX: {
      type: types.REAL,
      allowNull: false
    },
    OffsetY: {
      type: types.REAL,
      allowNull: false
    },
    OffsetZ: {
      type: types.REAL,
      allowNull: false
    },
    RespawnDelayMin: {
      type: types.INTEGER,
      allowNull: false
    },
    RespawnDelayMax: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefPath_FlockMember',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefPath_FlockMember__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
