export default (db, types) => db.define('_RefObjItemName', {
    RefObjItemID: {
      type: types.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    Name: {
      type: types.STRING(100),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefObjItemName',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefObjItemName",
        unique: true,
        fields: [
          { name: "RefObjItemID" },
        ]
      },
    ]
});
