export default (db, types) => db.define('_DelItem_N_RewardItemList', {
    DelCodeName128: {
      type: types.STRING(128),
      allowNull: false
    },
    DelRefItemID: {
      type: types.INTEGER,
      allowNull: false
    },
    RewardCodeName128: {
      type: types.STRING(128),
      allowNull: false
    },
    RewardRefItemID: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_DelItem_N_RewardItemList',
    schema: 'dbo',
    timestamps: false
});
