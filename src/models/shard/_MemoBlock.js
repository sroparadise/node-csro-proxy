export default (db, types) => db.define('_MemoBlock', {
    OwnerID: {
      type: types.INTEGER,
      allowNull: false,
      references: {
        model: '_Char',
        key: 'CharID'
      }
    },
    TargetCharName: {
      type: types.STRING(64),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_MemoBlock',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX_MemoBlock_OwnerID",
        fields: [
          { name: "OwnerID" },
        ]
      },
      {
        name: "IX_MemoBlock_TargetCharName",
        fields: [
          { name: "TargetCharName" },
        ]
      },
    ]
});
