export default (db, types) => db.define('Tab_RefNest', {
    dwNestID: {
      type: types.INTEGER,
      allowNull: false
    },
    dwHiveID: {
      type: types.INTEGER,
      allowNull: false
    },
    dwTacticsID: {
      type: types.INTEGER,
      allowNull: false
    },
    nRegionDBID: {
      type: types.SMALLINT,
      allowNull: false
    },
    fLocalPosX: {
      type: types.REAL,
      allowNull: true
    },
    fLocalPosY: {
      type: types.REAL,
      allowNull: true
    },
    fLocalPosZ: {
      type: types.REAL,
      allowNull: true
    },
    wInitialDir: {
      type: types.SMALLINT,
      allowNull: true
    },
    nRadius: {
      type: types.INTEGER,
      allowNull: true
    },
    nGenerateRadius: {
      type: types.INTEGER,
      allowNull: true
    },
    nChampionGenPercentage: {
      type: types.INTEGER,
      allowNull: true
    },
    dwDelayTimeMin: {
      type: types.INTEGER,
      allowNull: true
    },
    dwDelayTimeMax: {
      type: types.INTEGER,
      allowNull: true
    },
    dwMaxTotalCount: {
      type: types.INTEGER,
      allowNull: false
    },
    btFlag: {
      type: types.TINYINT,
      allowNull: true
    },
    btRespawn: {
      type: types.TINYINT,
      allowNull: false,
      defaultValue: 1
    },
    btType: {
      type: types.TINYINT,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize: db,
    tableName: 'Tab_RefNest',
    schema: 'dbo',
    timestamps: false
});
