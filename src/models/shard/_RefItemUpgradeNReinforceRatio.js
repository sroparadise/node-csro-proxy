export default (db, types) => db.define('_RefItemUpgradeNReinforceRatio', {
    RatioType: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    RatioType_Desc: {
      type: types.STRING(129),
      allowNull: false
    },
    RatioSubType: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    RatioSubType_Desc: {
      type: types.STRING(129),
      allowNull: false
    },
    Ratio: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefItemUpgradeNReinforceRatio',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefItemUpgradeNReinforceRatio",
        unique: true,
        fields: [
          { name: "RatioType" },
          { name: "RatioSubType" },
        ]
      },
    ]
});
