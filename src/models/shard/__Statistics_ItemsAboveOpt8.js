export default (db, types) => db.define('__Statistics_ItemsAboveOpt8', {
    nIdx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szType: {
      type: types.STRING(64),
      allowNull: true
    },
    szCharname16: {
      type: types.STRING(64),
      allowNull: true
    },
    szCodename128: {
      type: types.STRING(128),
      allowNull: true
    },
    nOptLevel: {
      type: types.INTEGER,
      allowNull: true
    },
    dEventTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__Statistics_ItemsAboveOpt8',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX___Statistics_ItemsAboveOpt8_dEventTime",
        fields: [
          { name: "dEventTime" },
        ]
      },
      {
        name: "PK____Statistics_Ite__2BFE89A6",
        unique: true,
        fields: [
          { name: "nIdx" },
        ]
      },
    ]
});
