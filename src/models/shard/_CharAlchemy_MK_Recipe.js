export default (db, types) => db.define('_CharAlchemy_MK_Recipe', {
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_Char',
        key: 'CharID'
      }
    },
    RC_ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    }
  }, {
    sequelize: db,
    tableName: '_CharAlchemy_MK_Recipe',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_CharAlchemy_MK_Recipe",
        unique: true,
        fields: [
          { name: "CharID" },
          { name: "RC_ID" },
        ]
      },
    ]
});
