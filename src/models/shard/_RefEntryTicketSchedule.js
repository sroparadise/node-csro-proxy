export default (db, types) => db.define('_RefEntryTicketSchedule', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    EventID: {
      type: types.INTEGER,
      allowNull: false
    },
    ScheduleType: {
      type: types.INTEGER,
      allowNull: false
    },
    StartDate: {
      type: "SMALLDATETIME",
      allowNull: false
    },
    EndDate: {
      type: "SMALLDATETIME",
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefEntryTicketSchedule',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefEntryTicketSchedule",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
