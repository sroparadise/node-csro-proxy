export default (db, types) => db.define('_BuyCashItemList_By_Web', {
    IDX: {
      type: types.BIGINT,
      allowNull: false
    },
    JID: {
      type: types.INTEGER,
      allowNull: false
    },
    Section: {
      type: types.INTEGER,
      allowNull: false
    },
    PackageCodeName: {
      type: types.STRING(128),
      allowNull: true
    },
    SubIDX: {
      type: types.BIGINT,
      allowNull: false
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: false
    },
    ItemCount: {
      type: types.INTEGER,
      allowNull: false
    },
    RegDate: {
      type: types.DATE,
      allowNull: false
    },
    RentType: {
      type: types.INTEGER,
      allowNull: true
    },
    CanDelete: {
      type: types.SMALLINT,
      allowNull: true
    },
    CanRecharge: {
      type: types.SMALLINT,
      allowNull: true
    },
    DateBegin: {
      type: "SMALLDATETIME",
      allowNull: true
    },
    DateEnd: {
      type: "SMALLDATETIME",
      allowNull: true
    },
    TimeCount: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_BuyCashItemList_By_Web',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__BuyCashItemList_By_Web_JID",
        fields: [
          { name: "JID" },
        ]
      },
    ]
});
