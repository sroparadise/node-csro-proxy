export default (db, types) => db.define('_User', {
  UserJID: {
    type: types.INTEGER,
    allowNull: false
  },
  CharID: {
    type: types.INTEGER,
    allowNull: false,
    primaryKey: true
  }
}, {
  sequelize: db,
  tableName: '_User',
  schema: 'dbo',
  timestamps: false,
  indexes: [
    {
      name: "IX__User",
      fields: [
        { name: "UserJID" },
      ]
    },
  ]
});
