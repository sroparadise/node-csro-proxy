export default (db, types) => db.define('_RefPath_Const', {
    ConstType: {
      type: types.STRING(32),
      allowNull: false
    },
    Codename: {
      type: types.STRING(128),
      allowNull: false,
      primaryKey: true
    },
    Description: {
      type: types.STRING(128),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefPath_Const',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefPath_Const__Codename",
        unique: true,
        fields: [
          { name: "Codename" },
        ]
      },
    ]
});
