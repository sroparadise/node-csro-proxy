export default (db, types) => db.define('_RefNonSplitItem', {
    RefItemID: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefNonSplitItem',
    schema: 'dbo',
    timestamps: false
});
