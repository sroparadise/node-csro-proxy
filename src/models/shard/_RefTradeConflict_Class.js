export default (db, types) => db.define('_RefTradeConflict_Class', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    ClassID: {
      type: types.TINYINT,
      allowNull: false
    },
    Description128: {
      type: types.STRING(129),
      allowNull: false
    },
    ReputationPointForRankUp: {
      type: types.BIGINT,
      allowNull: false
    },
    PassiveSkill1: {
      type: types.STRING(129),
      allowNull: false
    },
    PassiveSkill2: {
      type: types.STRING(129),
      allowNull: false
    },
    PassiveSkill3: {
      type: types.STRING(129),
      allowNull: false
    },
    PassiveSkill4: {
      type: types.STRING(129),
      allowNull: false
    },
    PassiveSkill5: {
      type: types.STRING(129),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefTradeConflict_Class',
    schema: 'dbo',
    timestamps: false
});
