export default (db, types) => db.define('_RefGachaTreeForClientUI', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    Tid1: {
      type: types.TINYINT,
      allowNull: false
    },
    Tid2: {
      type: types.TINYINT,
      allowNull: false
    },
    Tid3: {
      type: types.TINYINT,
      allowNull: false
    },
    Tid4: {
      type: types.TINYINT,
      allowNull: false
    },
    First_CategoryID: {
      type: types.SMALLINT,
      allowNull: false
    },
    First_Category_String: {
      type: types.STRING(128),
      allowNull: true
    },
    Second_CategoryID: {
      type: types.SMALLINT,
      allowNull: false
    },
    Second_Category_String: {
      type: types.STRING(128),
      allowNull: true
    },
    ItemDegree: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefGachaTreeForClientUI',
    schema: 'dbo',
    timestamps: false
});
