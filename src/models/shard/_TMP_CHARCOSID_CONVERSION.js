export default (db, types) => db.define('_TMP_CHARCOSID_CONVERSION', {
    ShardID: {
      type: types.SMALLINT,
      allowNull: false,
      primaryKey: true
    },
    OldID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    NewID: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_TMP_CHARCOSID_CONVERSION',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__TMP_CHARCOSID_CONVERSION",
        unique: true,
        fields: [
          { name: "ShardID" },
          { name: "OldID" },
        ]
      },
    ]
});
