export default (db, types) => db.define('_ConsignmentTrade_Progress_RuntimeLog', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    EventTime: {
      type: types.DATE,
      allowNull: false
    },
    EventState: {
      type: types.TINYINT,
      allowNull: false
    },
    strDesc: {
      type: types.STRING(1024),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_ConsignmentTrade_Progress_RuntimeLog',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__ConsignmentTrade_Progress_RuntimeLog__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
