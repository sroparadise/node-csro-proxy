export default (db, types) => db.define('_RefAlchemy_MK_MaterialGroup', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    GroupCodeName: {
      type: types.STRING(129),
      allowNull: false,
      primaryKey: true
    },
    ItemCodeName: {
      type: types.STRING(129),
      allowNull: false,
      primaryKey: true
    },
    Require_Count: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefAlchemy_MK_MaterialGroup',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_RefAlchemy_MK_MaterialGroup",
        unique: true,
        fields: [
          { name: "GroupCodeName" },
          { name: "ItemCodeName" },
        ]
      },
    ]
});
