export default (db, types) => db.define('_RefNewTrade_RewardRatio', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    DepartureName: {
      type: types.STRING(129),
      allowNull: false
    },
    ArrivalName: {
      type: types.STRING(129),
      allowNull: false
    },
    RewardRatio: {
      type: types.REAL,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefNewTrade_RewardRatio',
    schema: 'dbo',
    timestamps: false
});
