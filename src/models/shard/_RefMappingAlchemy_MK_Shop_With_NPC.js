export default (db, types) => db.define('_RefMappingAlchemy_MK_Shop_With_NPC', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    NPC_CodeName: {
      type: types.STRING(129),
      allowNull: false,
      primaryKey: true
    },
    Shop_CodeName: {
      type: types.STRING(129),
      allowNull: false,
      primaryKey: true
    }
  }, {
    sequelize: db,
    tableName: '_RefMappingAlchemy_MK_Shop_With_NPC',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_RefMappingAlchemy_MK_Shop_With_NPC",
        unique: true,
        fields: [
          { name: "NPC_CodeName" },
          { name: "Shop_CodeName" },
        ]
      },
    ]
});
