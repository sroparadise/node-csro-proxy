export default (db, types) => db.define('_UserTradeConflictJob', {
    UserJID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    RegDate: {
      type: "SMALLDATETIME",
      allowNull: false
    },
    JobType: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_UserTradeConflictJob',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__UserTradeConflictJob",
        unique: true,
        fields: [
          { name: "UserJID" },
        ]
      },
    ]
});
