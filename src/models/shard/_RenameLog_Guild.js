export default (db, types) => db.define('_RenameLog_Guild', {
    OldName: {
      type: types.STRING(64),
      allowNull: false,
      primaryKey: true
    },
    NewName: {
      type: types.STRING(64),
      allowNull: false
    },
    RenameTime: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RenameLog_Guild',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__RenameLog_Guild",
        fields: [
          { name: "NewName" },
        ]
      },
      {
        name: "PK__RenameLog_Guild",
        unique: true,
        fields: [
          { name: "OldName" },
        ]
      },
    ]
});
