export default (db, types) => db.define('_RefPricePolicyOfItem_2', {
    Service: {
      type: types.TINYINT,
      allowNull: false,
      defaultValue: 0
    },
    Country: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    RefPackageItemCodeName: {
      type: types.STRING(129),
      allowNull: false,
      primaryKey: true
    },
    PaymentDevice: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    PreviousCost: {
      type: types.INTEGER,
      allowNull: false
    },
    Cost: {
      type: types.INTEGER,
      allowNull: false
    },
    Param1: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Param1_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param2: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Param2_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param3: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Param3_Desc128: {
      type: types.STRING(129),
      allowNull: true,
      defaultValue: "xxx"
    },
    Param4: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Param4_Desc128: {
      type: types.STRING(129),
      allowNull: false,
      defaultValue: "xxx"
    }
  }, {
    sequelize: db,
    tableName: '_RefPricePolicyOfItem_2',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefPricePolicyOfItem_2",
        unique: true,
        fields: [
          { name: "RefPackageItemCodeName" },
          { name: "PaymentDevice" },
          { name: "Country" },
        ]
      },
    ]
});
