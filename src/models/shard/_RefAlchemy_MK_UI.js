export default (db, types) => db.define('_RefAlchemy_MK_UI', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    UI_Tab: {
      type: types.TINYINT,
      allowNull: false
    },
    UI_Tab_StrID128: {
      type: types.STRING(129),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefAlchemy_MK_UI',
    schema: 'dbo',
    timestamps: false
});
