export default (db, types) => db.define('_LOG_DEL_ITEM_N_REWARD_AMOUNT', {
    RewardDate: {
      type: "SMALLDATETIME",
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('getdate')
    },
    StorageType: {
      type: types.STRING(64),
      allowNull: false
    },
    OwnerID: {
      type: types.INTEGER,
      allowNull: false
    },
    ItemCodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    ID64: {
      type: types.BIGINT,
      allowNull: false
    },
    RewardType: {
      type: types.TINYINT,
      allowNull: false
    },
    Cnt: {
      type: types.INTEGER,
      allowNull: false
    },
    RewardAmount: {
      type: types.INTEGER,
      allowNull: false
    },
    TotalRewardAmount: {
      type: types.INTEGER,
      allowNull: false
    },
    IsReward: {
      type: types.STRING(2),
      allowNull: false
    },
    strDesc: {
      type: types.STRING(129),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_LOG_DEL_ITEM_N_REWARD_AMOUNT',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "_LOG_DEL_ITEM_N_REWARD_AMOUNT_INDEX",
        fields: [
          { name: "StorageType" },
          { name: "OwnerID" },
        ]
      },
    ]
});
