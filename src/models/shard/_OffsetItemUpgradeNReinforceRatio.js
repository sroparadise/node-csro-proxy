export default (db, types) => db.define('_OffsetItemUpgradeNReinforceRatio', {
    RatioType: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    RatioSubType: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    Ratio: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_OffsetItemUpgradeNReinforceRatio',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__OffsetItemUpgradeNReinforceRatio",
        unique: true,
        fields: [
          { name: "RatioType" },
          { name: "RatioSubType" },
        ]
      },
    ]
});
