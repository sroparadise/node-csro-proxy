export default (db, types) => db.define('_RefGift_ExchangeItem', {
    CouponIndex: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_RefGift_ExchangeCoupon',
        key: 'CouponIndex'
      }
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_RefObjCommon',
        key: 'ID'
      }
    },
    ItemStack: {
      type: types.SMALLINT,
      allowNull: false
    },
    ItemCount: {
      type: types.INTEGER,
      allowNull: false
    },
    ItemRatio: {
      type: types.REAL,
      allowNull: false
    },
    MagicType: {
      type: types.TINYINT,
      allowNull: false
    },
    MagicValue: {
      type: types.STRING(129),
      allowNull: false
    },
    RentCodeName: {
      type: types.STRING(129),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefGift_ExchangeItem',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefGift_ExchangeItem",
        unique: true,
        fields: [
          { name: "CouponIndex" },
          { name: "RefItemID" },
        ]
      },
    ]
});
