export default (db, types) => db.define('_RefEventMagicLampReward', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    ItemCodeName: {
      type: types.TEXT,
      allowNull: false
    },
    LevelData: {
      type: types.INTEGER,
      allowNull: true
    },
    Ratio: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefEventMagicLampReward',
    schema: 'dbo',
    timestamps: false
});
