export default (db, types) => db.define('_RefPath', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Codename: {
      type: types.STRING(128),
      allowNull: false,
      unique: "UK__RefPath__Codename"
    },
    Name: {
      type: types.STRING(128),
      allowNull: false,
      unique: "UK__RefPath__Name"
    },
    RelatedWorldID: {
      type: types.INTEGER,
      allowNull: false
    },
    ObjectType: {
      type: types.STRING(128),
      allowNull: false,
      references: {
        model: '_RefPath_Const',
        key: 'Codename'
      }
    },
    SpawnRange: {
      type: types.SMALLINT,
      allowNull: false
    },
    MoveSpeed: {
      type: types.SMALLINT,
      allowNull: false
    },
    RespawnType: {
      type: types.STRING(128),
      allowNull: false,
      references: {
        model: '_RefPath_Const',
        key: 'Codename'
      }
    },
    BehaviorTypeOnMasterDead: {
      type: types.STRING(128),
      allowNull: false,
      references: {
        model: '_RefPath_Const',
        key: 'Codename'
      }
    }
  }, {
    sequelize: db,
    tableName: '_RefPath',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefPath__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "UK__RefPath__Codename",
        unique: true,
        fields: [
          { name: "Codename" },
        ]
      },
      {
        name: "UK__RefPath__Name",
        unique: true,
        fields: [
          { name: "Name" },
        ]
      },
    ]
});
