export default (db, types) => db.define('_RefLearnRecipeByReqType', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    RecipeCodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    ReqType: {
      type: types.TINYINT,
      allowNull: false
    },
    ReqValue: {
      type: types.INTEGER,
      allowNull: false
    },
    ReqSubType: {
      type: types.TINYINT,
      allowNull: false
    },
    ReqSubValue: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefLearnRecipeByReqType',
    schema: 'dbo',
    timestamps: false
});
