export default (db, types) => db.define('_RefUpgradeEquipItem', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    BeforeRefID: {
      type: types.INTEGER,
      allowNull: false
    },
    BeforeOptLevel: {
      type: types.TINYINT,
      allowNull: false
    },
    UpgradeRefID: {
      type: types.INTEGER,
      allowNull: false
    },
    UpgradeOptLevel: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefUpgradeEquipItem',
    schema: 'dbo',
    timestamps: false
});
