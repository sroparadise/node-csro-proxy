export default (db, types) => db.define('_EventTicket', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: true
    },
    ItemNumber: {
      type: types.INTEGER,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_EventTicket',
    schema: 'dbo',
    timestamps: false
});
