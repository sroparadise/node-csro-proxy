export default (db, types) => db.define('_RefSkillGroup', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Code: {
      type: types.STRING(128),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefSkillGroup',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK___RefSkillGroup__6477ECF3",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
