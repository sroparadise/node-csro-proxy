export default (db, types) => db.define('_TradeEquipInventory', {
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_Char',
        key: 'CharID'
      }
    },
    Slot: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    ItemID: {
      type: types.BIGINT,
      allowNull: false,
      references: {
        model: '_Items',
        key: 'ID64'
      }
    }
  }, {
    sequelize: db,
    tableName: '_TradeEquipInventory',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__TradeEquipInventory",
        unique: true,
        fields: [
          { name: "CharID" },
          { name: "Slot" },
        ]
      },
    ]
});
