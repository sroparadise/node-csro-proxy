export default (db, types) => db.define('_RefTradeConflict_JobLevel', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    JobLevel: {
      type: types.TINYINT,
      allowNull: false
    },
    JobExp: {
      type: types.BIGINT,
      allowNull: false
    },
    PromotionReq: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefTradeConflict_JobLevel',
    schema: 'dbo',
    timestamps: false
});
