export default (db, types) => db.define('_CharNameList', {
    CharName16: {
      type: types.STRING(64),
      allowNull: false,
      primaryKey: true
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    }
  }, {
    sequelize: db,
    tableName: '_CharNameList',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__CHARNAMELIST",
        unique: true,
        fields: [
          { name: "CharName16" },
          { name: "CharID" },
        ]
      },
    ]
});
