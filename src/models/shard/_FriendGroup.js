export default (db, types) => db.define('_FriendGroup', {
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    GroupID: {
      type: types.SMALLINT,
      allowNull: false
    },
    GroupName: {
      type: types.STRING(30),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_FriendGroup',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "NC_IDX_FG_CharID",
        fields: [
          { name: "CharID" },
        ]
      },
      {
        name: "NC_IDX_FG_CharID_GroupID",
        fields: [
          { name: "CharID" },
          { name: "GroupID" },
        ]
      },
    ]
});
