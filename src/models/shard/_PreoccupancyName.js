export default (db, types) => db.define('_PreoccupancyName', {
    Name: {
      type: types.STRING(64),
      allowNull: false
    },
    NameType: {
      type: types.TINYINT,
      allowNull: false
    },
    OwnerID: {
      type: types.INTEGER,
      allowNull: false
    },
    ReqTime: {
      type: "SMALLDATETIME",
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_PreoccupancyName',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__PreoccupancyName_Name",
        fields: [
          { name: "NameType" },
          { name: "Name" },
        ]
      },
    ]
});
