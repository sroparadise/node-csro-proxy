export default (db, types) => db.define('_LOG_RESULT_REWARD_SILK', {
    RewardDate: {
      type: "SMALLDATETIME",
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('getdate')
    },
    JID: {
      type: types.INTEGER,
      allowNull: false
    },
    ErrorCode: {
      type: types.TINYINT,
      allowNull: false
    },
    RewardSilk: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_LOG_RESULT_REWARD_SILK',
    schema: 'dbo',
    timestamps: false
});
