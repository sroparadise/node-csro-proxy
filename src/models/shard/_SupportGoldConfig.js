export default (db, types) => db.define('_SupportGoldConfig', {
    ID: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    ManageType: {
      type: types.TINYINT,
      allowNull: true
    },
    MaximumGold: {
      type: types.BIGINT,
      allowNull: true
    },
    SupplyGold: {
      type: types.BIGINT,
      allowNull: true
    },
    ChargeGold: {
      type: types.INTEGER,
      allowNull: true
    },
    Charge_x1: {
      type: types.REAL,
      allowNull: true
    },
    Charge_x2: {
      type: types.REAL,
      allowNull: true
    },
    Charge_x3: {
      type: types.REAL,
      allowNull: true
    },
    Charge_x5: {
      type: types.REAL,
      allowNull: true
    },
    Charge_x10: {
      type: types.REAL,
      allowNull: true
    },
    Charge_x20: {
      type: types.REAL,
      allowNull: true
    },
    Charge_x30: {
      type: types.REAL,
      allowNull: true
    },
    Charge_x50: {
      type: types.REAL,
      allowNull: true
    },
    Charge_x100: {
      type: types.REAL,
      allowNull: true
    },
    Charge_x1000: {
      type: types.REAL,
      allowNull: true
    },
    AccumulatedGold: {
      type: types.BIGINT,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_SupportGoldConfig',
    schema: 'dbo',
    timestamps: false
});
