export default (db, types) => db.define('_EquipInvCos', {
    NewCOSID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Slot: {
      type: types.TINYINT,
      allowNull: false,
      primaryKey: true
    },
    ItemID: {
      type: types.BIGINT,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '_EquipInvCos',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__EquipInvCos",
        unique: true,
        fields: [
          { name: "NewCOSID" },
          { name: "Slot" },
        ]
      },
    ]
});
