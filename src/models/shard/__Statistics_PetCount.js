export default (db, types) => db.define('__Statistics_PetCount', {
    nIdx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szCodename128: {
      type: types.STRING(128),
      allowNull: true
    },
    nCount: {
      type: types.INTEGER,
      allowNull: true
    },
    dEventTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__Statistics_PetCount',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX___Statistics_PetCount_dEventTime",
        fields: [
          { name: "dEventTime" },
        ]
      },
      {
        name: "PK____Statistics_Pet__4865BE2A",
        unique: true,
        fields: [
          { name: "nIdx" },
        ]
      },
    ]
});
