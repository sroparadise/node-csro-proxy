export default (db, types) => db.define('_GuildMember', {
    GuildID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_Guild',
        key: 'ID'
      }
    },
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_Char',
        key: 'CharID'
      }
    },
    CharName: {
      type: types.STRING(64),
      allowNull: false
    },
    MemberClass: {
      type: types.TINYINT,
      allowNull: false
    },
    CharLevel: {
      type: types.TINYINT,
      allowNull: false
    },
    GP_Donation: {
      type: types.INTEGER,
      allowNull: false
    },
    JoinDate: {
      type: "SMALLDATETIME",
      allowNull: false
    },
    Permission: {
      type: types.INTEGER,
      allowNull: false
    },
    Contribution: {
      type: types.INTEGER,
      allowNull: false
    },
    GuildWarKill: {
      type: types.INTEGER,
      allowNull: false
    },
    GuildWarKilled: {
      type: types.INTEGER,
      allowNull: false
    },
    Nickname: {
      type: types.STRING(64),
      allowNull: true
    },
    RefObjID: {
      type: types.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    SiegeAuthority: {
      type: types.TINYINT,
      allowNull: true,
      defaultValue: 0
    },
    LatestRegion: {
      type: types.SMALLINT,
      allowNull: false,
      defaultValue: 0
    },
    LastLogout: {
      type: "SMALLDATETIME",
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('getdate')
    },
    LastLevelup: {
      type: "SMALLDATETIME",
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('getdate')
    }
  }, {
    sequelize: db,
    tableName: '_GuildMember',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__GuildMember",
        unique: true,
        fields: [
          { name: "GuildID" },
          { name: "CharID" },
        ]
      },
    ]
});
