export default (db, types) => db.define('_CharTradeConflictJob', {
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    NickNameRegDate: {
      type: "SMALLDATETIME",
      allowNull: false
    },
    JobLevel: {
      type: types.TINYINT,
      allowNull: false
    },
    JobExp: {
      type: types.BIGINT,
      allowNull: false
    },
    PromotionPhase: {
      type: types.TINYINT,
      allowNull: false
    },
    ReputationPoint: {
      type: types.BIGINT,
      allowNull: false
    },
    KillCount: {
      type: types.INTEGER,
      allowNull: false
    },
    Class: {
      type: types.TINYINT,
      allowNull: false
    },
    Rank: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_CharTradeConflictJob',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__CharTradeConflictJob",
        unique: true,
        fields: [
          { name: "CharID" },
        ]
      },
    ]
});
