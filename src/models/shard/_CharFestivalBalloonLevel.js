export default (db, types) => db.define('_CharFestivalBalloonLevel', {
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_Char',
        key: 'CharID'
      }
    },
    RefBalloonID: {
      type: types.INTEGER,
      allowNull: false
    },
    BalloonLevel: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_CharFestivalBalloonLevel',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__CharFestivalBalloonLevel",
        unique: true,
        fields: [
          { name: "CharID" },
        ]
      },
    ]
});
