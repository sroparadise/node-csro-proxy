export default (db, types) => db.define('_RefPath_Position_Config', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ConfigGroupID: {
      type: types.INTEGER,
      allowNull: false,
      references: {
        model: '_RefPath_Position_Config_Group',
        key: 'ID'
      }
    },
    ConfigCodename: {
      type: types.STRING(128),
      allowNull: false,
      references: {
        model: '_RefPath_Config_Type',
        key: 'ConfigCodename'
      }
    },
    ValueCodename: {
      type: types.STRING(128),
      allowNull: false
    },
    ValueType: {
      type: types.STRING(16),
      allowNull: false
    },
    Value: {
      type: types.STRING(128),
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefPath_Position_Config',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefPath_Position_Config__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
