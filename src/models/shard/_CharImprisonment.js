export default (db, types) => db.define('_CharImprisonment', {
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Remaintime: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_CharImprisonment',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__CharImprisonment",
        unique: true,
        fields: [
          { name: "CharID" },
        ]
      },
    ]
});
