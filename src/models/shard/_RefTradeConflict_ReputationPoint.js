export default (db, types) => db.define('_RefTradeConflict_ReputationPoint', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    LevelDiff: {
      type: types.SMALLINT,
      allowNull: false
    },
    ReputationPoint: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefTradeConflict_ReputationPoint',
    schema: 'dbo',
    timestamps: false
});
