export default (db, types) => db.define('_Repair_Quest_List', {
    charname16: {
      type: types.STRING(64),
      allowNull: false
    },
    id: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    codename: {
      type: types.STRING(128),
      allowNull: false
    },
    descname: {
      type: types.STRING(128),
      allowNull: false
    },
    eventtime: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_Repair_Quest_List',
    schema: 'dbo',
    timestamps: false
});
