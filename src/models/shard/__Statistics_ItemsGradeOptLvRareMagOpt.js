export default (db, types) => db.define('__Statistics_ItemsGradeOptLvRareMagOpt', {
    nIdx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nMagOptID: {
      type: types.SMALLINT,
      allowNull: false
    },
    nGrade: {
      type: types.TINYINT,
      allowNull: true
    },
    nOptLevel: {
      type: types.TINYINT,
      allowNull: true
    },
    nAppliedCount: {
      type: types.INTEGER,
      allowNull: true
    },
    nNotAppliedCount: {
      type: types.INTEGER,
      allowNull: true
    },
    dEventTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__Statistics_ItemsGradeOptLvRareMagOpt',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX__Statistics_ItemsGradeOptLvRareMagOpt_dEventITime",
        fields: [
          { name: "dEventTime" },
        ]
      },
      {
        name: "pk_nidx",
        unique: true,
        fields: [
          { name: "nIdx" },
        ]
      },
    ]
});
