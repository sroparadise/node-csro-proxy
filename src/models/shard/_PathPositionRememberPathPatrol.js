export default (db, types) => db.define('_PathPositionRememberPathPatrol', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    PathID: {
      type: types.INTEGER,
      allowNull: false
    },
    FlockID: {
      type: types.INTEGER,
      allowNull: false
    },
    FlockMemberID: {
      type: types.INTEGER,
      allowNull: false
    },
    PositionIndex: {
      type: types.INTEGER,
      allowNull: false
    },
    HeadDirection: {
      type: types.INTEGER,
      allowNull: false
    },
    LastPosLinkFirstPosState: {
      type: types.TINYINT,
      allowNull: false
    },
    PathPatrolLoopType: {
      type: types.TINYINT,
      allowNull: false
    },
    CanAttackState: {
      type: types.TINYINT,
      allowNull: false
    },
    HP: {
      type: types.INTEGER,
      allowNull: false
    },
    MP: {
      type: types.INTEGER,
      allowNull: false
    },
    ConsignmentTradeID: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_PathPositionRememberPathPatrol',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK___RefPath_FlockMemeber_Data_Memory__ID",
        unique: true,
        fields: [
          { name: "ID" },
        ]
      },
    ]
});
