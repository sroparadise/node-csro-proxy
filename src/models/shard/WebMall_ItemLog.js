export default (db, types) => db.define('WebMall_ItemLog', {
    ID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    InfoID: {
      type: types.INTEGER,
      allowNull: true
    },
    StrUserId: {
      type: types.STRING(50),
      allowNull: true
    },
    ItemCode: {
      type: types.STRING(128),
      allowNull: true
    },
    ItemNum: {
      type: types.INTEGER,
      allowNull: true
    },
    InTime: {
      type: types.DATE,
      allowNull: true
    },
    State: {
      type: types.INTEGER,
      allowNull: true
    },
    ShardID: {
      type: types.INTEGER,
      allowNull: true
    },
    dummy_serial_number: {
      type: types.BIGINT,
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'WebMall_ItemLog',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IDX__ID_ItemLog",
        fields: [
          { name: "ID", order: "DESC" },
        ]
      },
      {
        name: "idx_InfoID_WebMall_ItemLog",
        fields: [
          { name: "InfoID" },
        ]
      },
    ]
});
