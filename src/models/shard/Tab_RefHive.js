export default (db, types) => db.define('Tab_RefHive', {
    dwHiveID: {
      type: types.INTEGER,
      allowNull: false
    },
    btKeepMonsterCountType: {
      type: types.TINYINT,
      allowNull: true
    },
    dwOverwriteMaxTotalCount: {
      type: types.INTEGER,
      allowNull: true
    },
    fMonsterCountPerPC: {
      type: types.REAL,
      allowNull: true
    },
    dwSpawnSpeedIncreaseRate: {
      type: types.INTEGER,
      allowNull: true
    },
    dwMaxIncreaseRate: {
      type: types.INTEGER,
      allowNull: true
    },
    btFlag: {
      type: types.TINYINT,
      allowNull: true
    },
    GameWorldID: {
      type: types.SMALLINT,
      allowNull: true,
      defaultValue: 1
    },
    HatchObjType: {
      type: types.SMALLINT,
      allowNull: true,
      defaultValue: 1
    },
    szDescString128: {
      type: types.STRING(128),
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: 'Tab_RefHive',
    schema: 'dbo',
    timestamps: false
});
