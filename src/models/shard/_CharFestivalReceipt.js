export default (db, types) => db.define('_CharFestivalReceipt', {
    CharID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: '_Char',
        key: 'CharID'
      }
    },
    FestivalReceipt: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_CharFestivalReceipt',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__CharFestivalReceipt",
        unique: true,
        fields: [
          { name: "CharID" },
        ]
      },
    ]
});
