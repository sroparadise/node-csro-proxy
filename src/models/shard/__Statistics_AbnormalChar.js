export default (db, types) => db.define('__Statistics_AbnormalChar', {
    nIdx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szCharname16: {
      type: types.STRING(64),
      allowNull: true
    },
    nCurLevel: {
      type: types.TINYINT,
      allowNull: true
    },
    nMaxLevel: {
      type: types.TINYINT,
      allowNull: true
    },
    nStrength: {
      type: types.SMALLINT,
      allowNull: true
    },
    nIntellect: {
      type: types.SMALLINT,
      allowNull: true
    },
    nRemainStat: {
      type: types.SMALLINT,
      allowNull: true
    },
    nDifference: {
      type: types.INTEGER,
      allowNull: true
    },
    dEventTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__Statistics_AbnormalChar',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX___Statistics_AbnormalChar_dEventTime",
        fields: [
          { name: "dEventTime" },
        ]
      },
      {
        name: "PK____Statistics_Abn__2FCF1A8A",
        unique: true,
        fields: [
          { name: "nIdx" },
        ]
      },
    ]
});
