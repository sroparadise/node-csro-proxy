export default (db, types) => db.define('_CharTradeConflictJob_Kill', {
    CharID: {
      type: types.INTEGER,
      allowNull: false
    },
    KilledCharID: {
      type: types.INTEGER,
      allowNull: false
    },
    EventTime: {
      type: types.DATE,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_CharTradeConflictJob_Kill',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX_CharTradeConflictJob_Kill",
        fields: [
          { name: "CharID" },
        ]
      },
    ]
});
