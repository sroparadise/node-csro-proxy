export default (db, types) => db.define('__Statistics_ItemsOptLevel', {
    nIdx: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    szType: {
      type: types.STRING(64),
      allowNull: true
    },
    nOptLevel: {
      type: types.TINYINT,
      allowNull: true
    },
    nCount: {
      type: types.INTEGER,
      allowNull: true
    },
    dEventTime: {
      type: "SMALLDATETIME",
      allowNull: true
    }
  }, {
    sequelize: db,
    tableName: '__Statistics_ItemsOptLevel',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX___Statistics_ItemsOptLevel_dEventTime",
        fields: [
          { name: "dEventTime" },
        ]
      },
      {
        name: "PK____Statistics_Ite__4A4E069C",
        unique: true,
        fields: [
          { name: "nIdx" },
        ]
      },
    ]
});
