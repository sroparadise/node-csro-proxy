export default (db, types) => db.define('Tab_RefAISkill', {
    TacticsID: {
      type: types.INTEGER,
      allowNull: false
    },
    SkillCodeName: {
      type: types.STRING(129),
      allowNull: false
    },
    ExcuteConditionType: {
      type: types.TINYINT,
      allowNull: false
    },
    Condition_min: {
      type: types.TINYINT,
      allowNull: false
    },
    Condition_max: {
      type: types.TINYINT,
      allowNull: false
    },
    Option: {
      type: types.INTEGER,
      allowNull: false
    },
    ExcuteConditionData: {
      type: types.INTEGER,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: 'Tab_RefAISkill',
    schema: 'dbo',
    timestamps: false
});
