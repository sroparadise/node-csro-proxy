export default (db, types) => db.define('_RefGift_ExchangeCoupon', {
    Service: {
      type: types.TINYINT,
      allowNull: false
    },
    CouponIndex: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    RefItemID: {
      type: types.INTEGER,
      allowNull: false,
      references: {
        model: '_RefObjCommon',
        key: 'ID'
      }
    },
    ExchangeType: {
      type: types.TINYINT,
      allowNull: false
    }
  }, {
    sequelize: db,
    tableName: '_RefGift_ExchangeCoupon',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefGift_ExchangeCoupon",
        unique: true,
        fields: [
          { name: "CouponIndex" },
        ]
      },
    ]
});
