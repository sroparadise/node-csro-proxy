export default (db, types) => db.define('_RefEntryTicket', {
    Service: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    EventGroupID: {
      type: types.INTEGER,
      allowNull: false
    },
    EventID: {
      type: types.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    EntryType: {
      type: types.INTEGER,
      allowNull: false
    },
    MinLevel: {
      type: types.INTEGER,
      allowNull: false
    },
    MaxLevel: {
      type: types.INTEGER,
      allowNull: false
    },
    CountryCondition: {
      type: types.INTEGER,
      allowNull: false
    },
    GenderCondition: {
      type: types.INTEGER,
      allowNull: false
    },
    LimitCount: {
      type: types.INTEGER,
      allowNull: false
    },
    WinnerCount: {
      type: types.INTEGER,
      allowNull: false
    },
    DrawingRatio: {
      type: types.REAL,
      allowNull: false
    },
    ItemCodeName: {
      type: types.STRING(128),
      allowNull: false
    },
    ItemPayCount: {
      type: types.INTEGER,
      allowNull: false
    },
    CoinType1: {
      type: types.STRING(128),
      allowNull: false,
      defaultValue: "xxx"
    },
    CoinQuantity1: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    CoinType2: {
      type: types.STRING(128),
      allowNull: false,
      defaultValue: "xxx"
    },
    CoinQuantity2: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    CoinType3: {
      type: types.STRING(128),
      allowNull: false,
      defaultValue: "xxx"
    },
    CoinQuantity3: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Param1_Desc: {
      type: types.STRING(128),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param1: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Param2_Desc: {
      type: types.STRING(128),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param2: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Param3_Desc: {
      type: types.STRING(128),
      allowNull: false,
      defaultValue: "xxx"
    },
    Param3: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize: db,
    tableName: '_RefEntryTicket',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK__RefEntryTicket",
        unique: true,
        fields: [
          { name: "EventID" },
        ]
      },
    ]
});
