import { QueryTypes } from 'sequelize';

const config = {
    path: `/mall-token`,
    method: `POST`,
};

const controller = async (req, res) => {
    const [{ JID, token }] = await req.database.query(
        `exec _web_mall_token ?`,
        {
            replacements: [
                req.body.StrUserID,
            ],
            type: QueryTypes.SELECT,
        },
    );

    res.status(200).send({
        JID, token,
    });
};

export default {
    config,
    controller,
};