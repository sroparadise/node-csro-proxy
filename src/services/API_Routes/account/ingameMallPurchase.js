import { QueryTypes } from 'sequelize';

const config = {
    path: `/mall-buy`,
    method: `POST`,
};

const controller = async (
    {
        body: {
            CharName16,
            package_code,
            count,
        },
        database,
    }, 
    res,
) => {
    const [{ result }] = await database.query(
        `exec _BUY_INGAME_MALL_ITEM ?, ?, ?`,
        {
            replacements: [
                CharName16,
                package_code,
                count,
            ],
            type: QueryTypes.SELECT,
        },
    );

    res.status(200).send({  
        result,
    });
};

export default {
    config,
    controller,
};