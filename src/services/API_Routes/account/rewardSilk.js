import { QueryTypes } from 'sequelize';

const config = {
    path: `/reward-silk`,
    method: `POST`,
};

const controller = async (
    {
        body: {
            UserJID,
            CharID,
            amount,
        },
        database,
    },
    res,
) => {
    try {
        await database.query(
            `exec _REWARD_SILK ?, ?, ?`,
            {
                replacements: [
                    UserJID,
                    CharID,
                    amount,
                ],
                type: QueryTypes.SELECT,
            },
        );
        res.status(200).end();
    } catch (e) {
        res.status(400).end();
    }
};

export default {
    config,
    controller,
};