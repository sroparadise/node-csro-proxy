import * as models from '@models/addon';
import WebServer from '@lib/WebServer';
import config from '@config/API';

class API_Addon extends WebServer {
    constructor() {
        super(config.addon, models);
    }
}

export default API_Addon;
